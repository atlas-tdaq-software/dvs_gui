#ifndef TESTING_THREAD_H
#define TESTING_THREAD_H

#include <QMutex>
#include <QThread>
#include <QWaitCondition>
#include <dvs/manager.h>
#include <dvs/dvscomponent.h>
#include <dvs_gui/signals.h>

namespace dvs_gui {

class TestingThread : public QThread
{
    Q_OBJECT

public:
    TestingThread(QObject *parent = 0);
    ~TestingThread();
    void getTestingParams(daq::dvs::Manager* manager, daq::dvs::Component* comp, std::string scope, int level);
    GUISignals* gs;
    static TestingThread* instance_;

protected:
    void run();

private:
    QMutex mutex;
    QWaitCondition condition;
    bool restart;
    bool abort;
    daq::dvs::Component* testing_comp;
    daq::dvs::Manager* manager;
    std::string scope;
    int level;

};
}
#endif
