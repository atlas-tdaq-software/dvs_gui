#ifndef SINGLE_TESTING_THREAD_H
#define SINGLE_TESTING_THREAD_H

#include <QMutex>
#include <QThread>
#include <QWaitCondition>
#include <dvs/manager.h>
#include <dvs/dvscomponent.h>
#include <dvs_gui/signals.h>
#include <dvs_gui/single_testing_thread.h>

namespace dvs_gui {

class SingleTestingThread : public QThread
{
    Q_OBJECT

public:
    SingleTestingThread(QObject *parent = 0);
    ~SingleTestingThread();
    void getTestingParams(daq::dvs::Manager* manager, daq::dvs::Component* comp, QString test_id, std::string scope, int level);
    GUISignals* gs;
    static SingleTestingThread* instance_;

protected:
    void run();

private:
    QMutex mutex;
    QWaitCondition condition;
    bool restart;
    bool abort;
    daq::dvs::Component* testing_comp;
    QString testID;
    daq::dvs::Manager* manager;
    std::string scope;
    int level;

};
}
#endif
