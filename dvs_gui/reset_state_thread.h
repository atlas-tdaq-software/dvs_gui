#ifndef RESET_STATE_THREAD_H
#define RESET_STATE_THREAD_H

#include <QMutex>
#include <QThread>
#include <QWaitCondition>
#include <dvs/manager.h>
#include <dvs/dvscomponent.h>

namespace dvs_gui {

class ResetStateThread : public QThread
{
    Q_OBJECT

public:
    ResetStateThread(QObject *parent = 0);
    ~ResetStateThread();
    void getResetParams(daq::dvs::Manager* manager, daq::dvs::Component* comp);

protected:
    void run();

private:
    QMutex mutex;
    QWaitCondition condition;
    bool restart;
    bool abort;
    daq::dvs::Component* reset_comp;
    daq::dvs::Manager* manager;
};
}
#endif
