/*
 * output_window.h
 *
 *  Created on: Feb 2, 2009
 *      Author: alina
 */

#ifndef OUTPUT_WINDOW_H
#define OUTPUT_WINDOW_H

#include <QWidget>


class QPushButton;
class QTextEdit;

class OutputWindow : public QWidget
{
	Q_OBJECT

public:
	OutputWindow(QWidget *parent = 0, std::string part="");
	~OutputWindow();
        QString getDVSOutputFile();

public slots:
        void printContent();

private:
	QTextEdit *textEdit;
	QPushButton *closeButton;
        std::string partition_name;

};

#endif /* OUTPUT_WINDOW_H */
