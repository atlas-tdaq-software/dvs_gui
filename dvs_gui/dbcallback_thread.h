#ifndef DB_CALLBACK_THREAD_H
#define DB_CALLBACK_THREAD_H

#include <QMutex>
#include <QThread>
#include <QWaitCondition>
#include <config/Configuration.h>
#include <config/SubscriptionCriteria.h>

namespace dvs_gui {

class DBCallbackThread : public QThread
{
    Q_OBJECT

public:
    DBCallbackThread(QObject *parent = 0);
    ~DBCallbackThread();
    void getSubscriptionParams(Configuration* config);
    
signals:
    void finish_notify();
    
protected:
    void run();

private:
    QMutex mutex;
    QWaitCondition condition;
    bool restart;
    bool abort;
    static void dbCallback(const std::vector<ConfigurationChange *> &, void *);
    ConfigurationSubscriptionCriteria criteria_;
    Configuration* config_;
    static DBCallbackThread* instance_;
};
}
#endif
