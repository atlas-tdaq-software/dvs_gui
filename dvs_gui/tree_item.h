/****************************************************************************
 **
 ** Copyright (C) 2005-2008 Trolltech ASA. All rights reserved.
 **
 ** This file is part of the documentation of the Qt Toolkit.
 **
 ****************************************************************************/

 #ifndef TREEITEM_H
 #define TREEITEM_H

 #include <QList>
 #include <QVariant>
 #include <dvs/dvscomponent.h>

namespace dvs_gui {

 class TreeItem
 {
 public:
     TreeItem(const QVariant &data, const std::string & key, TreeItem *parent = 0);
     ~TreeItem();

     void appendChild(TreeItem *child);
     TreeItem* child(int row);
     int childCount() const;
     int columnCount() const;
     QVariant data() const;
     int row() const;
     TreeItem* parent();

     daq::dvs::Component::Status getStatus() { return status_; }
     void setStatus(daq::dvs::Component::Status s) { status_ = s;}

     uint32_t getIndividualTestResult() { return indivResult_; }
     void setIndividualTestResult(uint32_t s) { indivResult_ = s;}

     const std::string & getKey() const { return key_; }

     std::string getTest_out();
     void setTest_out(std::string r);
     std::string getTest_err();
     void setTest_err(std::string r);

     std::string getDiagnosis();
     void setDiagnosis(std::string d);

     std::string getReason();
     void setReason(std::string re);

     QList<TreeItem*> childItems;
     std::string buff;

 private:
     QVariant itemData;
     TreeItem* parentItem;
     daq::dvs::Component::Status status_;
     std::string key_;
     std::string result_;
     std::string diagnosis_;
     std::string reason_;
     std::string test_out;
     std::string test_err;
     uint32_t indivResult_;
 };

} //namespace dvs_gui

#endif
