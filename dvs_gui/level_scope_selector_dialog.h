 #ifndef LEVELSCOPESELECTOR_H
 #define LEVELSCOPESELECTOR_H

 #include <QWidget>
 #include <QDialog>
 #include <QComboBox>
 #include <QCheckBox>
 #include <QDialogButtonBox>

 namespace dvs_gui {

 class LevelScopeSelector : public QDialog
 {
     Q_OBJECT

 public:
     LevelScopeSelector(QWidget* parent =0, int prev_level=255, QString prev_scope="any") ;

     QComboBox* comboBox_levelsel;
     QComboBox* comboBox_scope;
     QDialogButtonBox* buttonBox;

  };

 } //namespace dvs_gui
 #endif
