 #ifndef DVSTREEPANEL_H
 #define DVSTREEPANEL_H

 #include <QWidget>
 #include <QTreeView>
 #include <QGridLayout>  
 //#include <QSortFilterProxyModel>

 #include "ui_dvs_tree_panel.h" 
 #include <dvs/dvscomponent.h>
 #include <dvs_gui/tree_model.h>
 #include <dvs_gui/my_treeview.h>


 namespace dvs_gui {
 
 class DVSTreePanel : public QWidget, private Ui::dvs_tree_panel
 {
     Q_OBJECT

 public:
     DVSTreePanel(QWidget* parent =0) ;
     void loadDVSComponentTree(std::shared_ptr<daq::dvs::Component> thetree, const char* part, daq::dvs::Manager* manager);
     void cleanup();
     MyTreeView* treeView;
     TreeModel* model;
     //TreeModelProxy *proxyModel;

 private:
     QGridLayout* layout;

  };

 } //namespace dvs_gui
 #endif
