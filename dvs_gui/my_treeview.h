 #ifndef MYTREEVIEW_H
 #define MYTREEVIEW_H

 #include <QWidget>
 #include <QTreeView>

 namespace dvs_gui {
 
 class MyTreeView : public QTreeView
 {
     Q_OBJECT

 // void mouseDoubleClickEvent(QMouseEvent*) override ;

 public:
     MyTreeView(QWidget* parent =0) ;
     void contextMenuEvent(QContextMenuEvent *event);

 public slots:
    void expandSubtree(const QModelIndex&) ;
    void expandToFailed(const QModelIndex&) ;

 signals:
     void giveMePosition(const QPoint & );

 private:
     void expandAllChildren(const QModelIndex&, bool);

  };

 } //namespace dvs_gui
 #endif
