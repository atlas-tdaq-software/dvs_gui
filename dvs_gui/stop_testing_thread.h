#ifndef STOP_TESTING_THREAD_H
#define STOP_TESTING_THREAD_H

#include <QMutex>
#include <QThread>
#include <QWaitCondition>
#include <dvs/manager.h>
#include <dvs/dvscomponent.h>

namespace dvs_gui {

class StopTestingThread : public QThread
{
    Q_OBJECT

public:
    StopTestingThread(QObject *parent = 0);
    ~StopTestingThread();
    void getStopParams(daq::dvs::Manager* manager, daq::dvs::Component* comp);

protected:
    void run();

private:
    QMutex mutex;
    QWaitCondition condition;
    bool restart;
    bool abort;
    daq::dvs::Component* stop_comp;
    daq::dvs::Manager* manager;
};
}
#endif
