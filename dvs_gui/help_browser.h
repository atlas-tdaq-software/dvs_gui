/*
 * help_browser.h
 *
 *  Created on: Feb 26, 2009
 *      Author: alina
 */

#ifndef HELP_BROWSER_H_
#define HELP_BROWSER_H_

#include <QtCore/QString>
#include <QTextBrowser>
#include <QPushButton>
#include <QDir>

class HelpBrowser : public QWidget
{
	Q_OBJECT

public:
	HelpBrowser(const QString &path, const QString &page, QWidget *parent=0);
    static void showPage(const QString &page);

private slots:
    void updateWindowTitle();

private:
	static QDir directoryOf(const QString& subdir);
	QTextBrowser* textBrowser;
	QPushButton* homeButton;
	QPushButton* backButton;
	QPushButton* exitButton;

};
#endif /* HELP_BROWSER_H_ */
