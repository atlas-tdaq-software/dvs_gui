 #ifndef DVSMAINWINDOW_H
 #define DVSMAINWINDOW_H

 #include "ui_dvs_mainwindow.h"
 #include <config/Configuration.h>
 #include <dvs/manager.h>
 #include <dvs/dvscomponent.h>
 #include <dvs_gui/testing_thread.h>
 #include <dvs_gui/stop_testing_thread.h>
 #include <dvs_gui/reset_state_thread.h>
 #include <dvs_gui/single_testing_thread.h>
 #include <QTUtils/highlighter.h>
 #include <config/SubscriptionCriteria.h>
 #include <dvs_gui/dbcallback_thread.h>

 namespace dvs_gui {

 class DVSMainWindow : public QMainWindow, private Ui::MainWindow
 {
     Q_OBJECT

 public:
     DVSMainWindow(QWidget* parent =0, const char* db_name="", const char* part_name="") ;
     ~DVSMainWindow();
     static Configuration* config;
     daq::dvs::Manager* manager;
     std::shared_ptr<daq::dvs::Component> tree;
     
 public slots:
     void startTest();
     void startSingleTest(QAction* );
     void stopTest();
     void reloadDB(bool );
     void editDB();
     void resetState();
     void updateTabs(std::string );
     void updateTabs(int );
     void printInTab(const QModelIndex & );
     void showTests(const QPoint &);
     void changeLevelScope();
     void findFirst(const QString & );
     void findNext();
     void findPrevious();
     void highlightSearch();
     void on_refreshButton_clicked();
     void logViewerSettings();
     void on_saveTestResultButton_clicked();
     void on_browseButton_clicked();
     void on_actionViewDVSOutput_triggered();
     void on_filterButton_clicked();
     void on_actionCombineTestOutput_toggled(bool );
     //void on_actionEnableHardwareOFF_toggled(bool checked);
     void on_updateTestLog_clicked();
     void on_actionHelpMenu_triggered();
     void enableButtons(std::string comp );
     void on_actionTest_verbosity_toggled(bool );
     void on_actionDiscard_test_output_toggled(bool );
     void on_actionHWView_toggled(bool );

 private:
     void setupHighlighters();
     void closeEvent(QCloseEvent *event);
     const char* db_name_;
     const char* partition_name_;
     const char* logroot_;
     const char* initlogroot_;
     bool run_P1_;
     std::string current_node_key_;
     QMenu* testmenu_;
     int test_level;
     QString test_scope;
     TestingThread thread;
     StopTestingThread stop_thread;
     ResetStateThread reset_thread;
     SingleTestingThread s_thread;
     daq::QTUtils::Highlighter* highlighter;
     daq::QTUtils::Highlighter* highlighter_err;
     void loadDB();
     std::string getKeyFromCurrentIndex();
     daq::dvs::Component* getComponentFromCurrentIndex();
     QString getLogFile(std::string host,std::string name,std::string extension, daq::dvs::Component::Type t);
     std::string view_cmd;
     std::string end_view_cmd;
     int log_set;
     daq::QTUtils::Highlighter* highlighter_testout;
     daq::QTUtils::Highlighter* highlighter_testerr;
     QString fileName;
     int logPanelSelected();
     QString getTestLog(std::string host, std::string test_output, std::string ext);
     std::string comp_under_testing;
     std::string comp_under_stop;
     std::string comp_under_reset;
     void subscribeToDatabaseChanges();
     DBCallbackThread dbcallback_thread;
     std::string combineChildrenOutputForComponent(TreeItem* ,std::string );
     bool combineChildrenOutput;
     bool HWview;

 };

 } //namespace dvs_gui
 #endif
