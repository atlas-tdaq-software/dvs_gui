 /****************************************************************************
 **
 ** Copyright (C) 2005-2008 Trolltech ASA. All rights reserved.
 **
 ** This file is part of the documentation of the Qt Toolkit.
 **
 ****************************************************************************/

 #ifndef TREEMODEL_H
 #define TREEMODEL_H

 #include <QAbstractItemModel>
 #include <QModelIndex>
 #include <QModelIndexList>

 #include <QVariant>
 #include <QMap>
 #include <dvs/dvscomponent.h>
 #include <dvs_gui/tree_item.h>
 #include <dvs/manager.h>

namespace dvs_gui {

 class TreeModel : public QAbstractItemModel
 {
     Q_OBJECT

 public:
     TreeModel(std::shared_ptr<daq::dvs::Component> tree, const char* part_name, daq::dvs::Manager* manager, QObject *parent = 0);
     ~TreeModel();

     QVariant data(const QModelIndex &index, int role) const;
     Qt::ItemFlags flags(const QModelIndex &index) const;
     QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

     //virtual methods in QAbstractItemModel, needs declaration and implementation
     QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
     QModelIndex parent(const QModelIndex &index) const;
     int rowCount(const QModelIndex &parent = QModelIndex()) const;
     int columnCount(const QModelIndex &parent = QModelIndex()) const;

public slots:
     void treeStatusUpdate(daq::tmgr::TestResult status, std::string comp);
     void treeItemChanged(std::string comp_id);

public:
    signals:
    void testingCompleted(std::string );
	void resetDone(std::string );
	void itemUpdated(std::string );
    void failedComponent(const QModelIndex &) ;

private:
     void setupModelData(daq::dvs::Component* tree, TreeItem* root);
     QModelIndex getIndex(TreeItem* item, TreeItem* parent);
     QModelIndexList match(const QModelIndex &start, int role,const QString &value, int hits, Qt::MatchFlags flags) const;
     TreeItem* rootItem;
     QList<TreeItem*> parents;
     QList<daq::dvs::Component*> components;
     mutable QMultiHash<QString, TreeItem*> itemMap;
     bool markTestableItem(daq::dvs::Component* component);
     void markTestableParent(TreeItem* aParent);
     daq::dvs::Manager* manager;
     std::map<int, std::string> resultStr;
 };

} //namespace dvs_gui

#endif
