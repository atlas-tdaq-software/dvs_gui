#ifndef GUISIGNALS_H
#define GUISIGNALS_H

#include <QtCore/qobject.h>
#include <dvs/dvscomponent.h>

namespace dvs_gui {

class GUISignals : public QObject
{
    Q_OBJECT
 public:
    GUISignals(){};
    ~GUISignals(){};
    void emitterStatusChanged(daq::tmgr::TestResult , std::string );
    void emitterMessageChanged(std::string );

public:
    signals:
       void statusChanged(daq::tmgr::TestResult , std::string );
       void messageChanged(std::string );

 private:
} ;

} //namespace dvs_gui

#endif
