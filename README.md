## DVS - Diagnostic and Verification System (framework)

### User-defined icons

Icons can be (re)defined by users for every OKS class, e.g. HW_System or Computer. For each class, 4 icons must be supplied. Icons are supplied in a form of binary Qt resource file .rcc, which is built from a simple XML resource description file and the image files (png), like in `examples/usericons/user_icons.qrc` file:

```xml
<RCC>
    <qresource prefix="/icons" >
        <file alias="Computer/Untested">Computer/computer.png</file>
        <file alias="Computer/Passed">Computer/green_computer.png</file>
        <file alias="Computer/Failed">Computer/red_computer.png</file>
        <file alias="Computer/Unresolved">Computer/orange_computer.png</file>
    </qresource>
</RCC>
```
Aliases must correspond to `<classname>/<state>` where state is test result, i.e. one of *{Untested, Passed, Failed, Unresolved}*. The real directory and file names are irrelevant (but relative to .qrc file). 
Corresponding binary resource file is built with rcc compiler:
```shell
> $QTDIR/bin/rcc -binary examples/usericons/user_icons.qrc -o user_icons.rcc
```
and then passed to dvs_main like
```shell
> dvs_start_gui <partition>.data.xml <partition> user_icons.rcc
```
or by defining `TDAQ_DVS_USER_ICONS` enfironment viariable, or by keeping this fils as `$HOME/.tdaq/dvs/icons.rcc`
