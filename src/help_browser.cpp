/*
 * help_browser.cpp
 *
 *  Created on: Feb 26, 2009
 *      Author: alina
 */

#include <QtGui>
#include <QtWidgets>
#include <dvs_gui/help_browser.h>

HelpBrowser::HelpBrowser(const QString &path, const QString &page, QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_GroupLeader);
	textBrowser = new QTextBrowser;
	homeButton = new QPushButton(tr("Home"));
	backButton = new QPushButton(tr("Back"));
	exitButton = new QPushButton(tr("Exit"));

	QHBoxLayout* buttonLayout = new QHBoxLayout;
	buttonLayout->addWidget(homeButton);
	buttonLayout->addWidget(backButton);
	buttonLayout->addStretch();
	buttonLayout->addWidget(exitButton);

	QVBoxLayout* mainLayout = new QVBoxLayout;
	mainLayout->addLayout(buttonLayout);
	mainLayout->addWidget(textBrowser);
	setLayout(mainLayout);

	textBrowser->setSearchPaths(QStringList()<<path<<":/images/images");
	textBrowser->setSource(page);
	setWindowTitle(tr("Help: %1").arg(textBrowser->documentTitle()));
	
	connect(exitButton, SIGNAL(clicked()), this, SLOT(close()));
	connect(homeButton, SIGNAL(clicked()), textBrowser, SLOT(home()));
	connect(backButton, SIGNAL(clicked()), textBrowser, SLOT(backward()));

	connect(textBrowser, SIGNAL(sourceChanged(const QUrl &)), this, SLOT(updateWindowTitle()));
}

void HelpBrowser::updateWindowTitle()
{
	setWindowTitle(tr("Help: %1").arg(textBrowser->documentTitle()));
}

void HelpBrowser::showPage(const QString &page)
{
	QString path = directoryOf("share/doc/dvs_gui/help").absolutePath();
	HelpBrowser* browser = new HelpBrowser(path, page);
	browser->resize(500, 400);
	browser->show();
}

QDir HelpBrowser::directoryOf(const QString& subdir)
{
	//get Application path and cd ../../share/doc/dvs_gui/help
	QDir dir(QCoreApplication::applicationDirPath());
	dir.cdUp();
	dir.cdUp();
	dir.cd(subdir);
	return dir;
}
