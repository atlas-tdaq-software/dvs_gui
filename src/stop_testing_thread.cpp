 #include <QtGui>

 #include <dvs_gui/stop_testing_thread.h>
 #include <dvs/dvscomponent.h>

 using namespace daq::dvs;
 using namespace dvs_gui ;

 StopTestingThread::StopTestingThread(QObject *parent)
     : QThread(parent)
 {
     restart = false;
     abort = false;
 }

 StopTestingThread::~StopTestingThread()
 {
     mutex.lock();
     abort = true;
     condition.wakeOne();
     mutex.unlock();

     wait();
 }

 void StopTestingThread::getStopParams(Manager* manager, daq::dvs::Component* comp)
 {
     QMutexLocker locker(&mutex);

     this->stop_comp = comp;
     this->manager = manager;

     if (!isRunning()) {
         start(LowPriority);
     } else {
         restart = true;
         condition.wakeOne();
     }
 }

 void StopTestingThread::run()
 {
     forever {
         mutex.lock();
         Component* stop_comp__ = this->stop_comp;
         Manager* manager__ = this->manager;
         mutex.unlock();
std::cout<<"***Stop comp "<<stop_comp__->key()<<std::endl;
         manager__->stopTest(stop_comp__);

         if (restart)
              break;
         if (abort)
              return;

         mutex.lock();
         if (!restart)
            condition.wait(&mutex);
         restart = false;
         mutex.unlock();
     }
 }
