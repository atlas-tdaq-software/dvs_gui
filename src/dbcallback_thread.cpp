 #include <QtGui>

 #include <dvs_gui/dbcallback_thread.h>
 #include <ers/ers.h>

 using namespace dvs_gui ;

 DBCallbackThread * DBCallbackThread::instance_ = NULL;
 
 DBCallbackThread::DBCallbackThread(QObject *parent)
     : QThread(parent)
 {
     restart = false;
     abort = false;
     instance_=this;
 }

 DBCallbackThread::~DBCallbackThread()
 {
     mutex.lock();
     abort = true;
     condition.wakeOne();
     mutex.unlock();

     wait();
 }
   
 void DBCallbackThread::dbCallback(const std::vector<ConfigurationChange *> &, void *) 
 { 
    ERS_DEBUG(0,"Callback from db.");

    emit instance_->finish_notify();
 }

 void DBCallbackThread::getSubscriptionParams(Configuration* config)
 {
     QMutexLocker locker(&mutex);
     
     ConfigurationSubscriptionCriteria criteria;
     this->criteria_=criteria;
     this->config_=config;
     
     if (!isRunning()) {
         start(LowPriority);
     } else {
         restart = true;
         condition.wakeOne();
     }
 }

 void DBCallbackThread::run()
 {
     forever {
         mutex.lock();
         ConfigurationSubscriptionCriteria criteria__ = this->criteria_;
	 Configuration* config__ = this->config_;
	 mutex.unlock();

         config__->subscribe(criteria__, dbCallback);

         if (restart)
              break;
         if (abort)
              return;

         mutex.lock();
         if (!restart)
            condition.wait(&mutex);
         restart = false;
         mutex.unlock();
     }
 }
