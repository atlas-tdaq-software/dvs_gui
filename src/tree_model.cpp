/*
    tree_model.cpp
*/
#include <QMetaType>
#include <QtGui>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <dvs_gui/tree_item.h>
#include <dvs_gui/tree_model.h>
#include <algorithm>

using namespace daq::dvs;
using namespace dvs_gui;

Q_DECLARE_METATYPE(daq::dvs::Component*)

TreeModel::TreeModel(std::shared_ptr<daq::dvs::Component> tree, const char* part_name, Manager* manager, QObject *parent)
        : QAbstractItemModel(parent)
{
    this->manager=manager;
    if(tree->dependencies().size()==0){
    	throw CannotLoadDatabase(ERS_HERE, "No component is testable. Please change your database configuration.");
    }

    //To display part object root item, need to create a fake root node and to make it
    //parent of our tree root node. The fake root is a copy of the first element

    daq::dvs::Component* root_fake_comp=tree.get();
    QVariant fakeRootData;
    qVariantSetValue(fakeRootData,root_fake_comp);
    rootItem = new TreeItem(fakeRootData,"fake root");

    QVariant variant;
    qVariantSetValue(variant,tree.get());
    TreeItem* newItem = new TreeItem(variant, tree.get()->key(), rootItem);
    itemMap.insert(QString(tree.get()->key().c_str()),newItem);
    newItem->setStatus(daq::dvs::Component::Status::UNDEFINED);
    newItem->setIndividualTestResult(daq::tmgr::TmUndef);
    rootItem->appendChild(newItem);
    parents<<newItem;

    setupModelData(tree.get(),newItem);

    ERS_LOG("How many components? "<<components.size());
    components<<tree.get();

    /*foreach (daq::dvs::Component* cc, components){
      std::cout << cc->key() <<" "<<cc->type()<< std::endl;
    }*/
    resultStr[daq::tmgr::TmPass]="Passed";
    resultStr[daq::tmgr::TmUndef] = "Undef";
    resultStr[daq::tmgr::TmFail] = "Failed";
    resultStr[daq::tmgr::TmUnresolved] = "Unresolved";
    resultStr[daq::tmgr::TmUntested] = "Untested";
    resultStr[daq::tmgr::TmUnsupported] = "Unsupported";

}

TreeModel::~TreeModel()
{
    delete rootItem;
    components.clear();
}

void TreeModel::setupModelData(daq::dvs::Component* tree, TreeItem* parent)
{
    parents << parent;

//    auto cit = tree->dependencies().begin() ;
    for ( auto cit: tree->dependencies() )
    {
        daq::dvs::Component* kid = cit ;
        components<<kid;

        QVariant variant;
        qVariantSetValue(variant,kid);
        TreeItem* newItem = new TreeItem(variant, kid->key(), parent);
        itemMap.insert(QString(kid->key().c_str()),newItem);
        newItem->setStatus(daq::dvs::Component::Status::UNDEFINED);
        newItem->setIndividualTestResult(daq::tmgr::TmUndef);
        parent->appendChild(newItem);
		
        if((kid->dependencies()).size()>0)
        {
           parents << newItem;
           setupModelData(kid,parents.last());
        }
        else {
	   //mark only the leaf items if they are nontestable
	   if(!markTestableItem(kid)){
	      newItem->setStatus(daq::dvs::Component::Status::UNDEFINED);
	      newItem->setIndividualTestResult(daq::tmgr::TmUndef);

	   }
        }
	markTestableParent(parent);	
    }
}

void TreeModel::markTestableParent(TreeItem* aParent)
{
	//std::cout<<"parent "<<aParent->getKey()<<" "<<aParent->getStatus()<<std::endl;
 //analyse if all children are nontestable, mark the container as nontestable as well.    
    for (int i=0; i<aParent->childCount(); i++) {
       if( (aParent->childItems[i])->getStatus() != Component::UNSUPPORTED ){
            aParent->setStatus(daq::dvs::Component::Status::UNSUPPORTED);
	    return;
       }  
    }
    aParent->setStatus(daq::dvs::Component::Status::UNDEFINED);
    return;
}

bool TreeModel::markTestableItem(daq::dvs::Component* component)
{
    Manager* manager__ = this->manager;
    TestsSet tests_for_obj ;
    // daq::tmgr::TestsConfiguration::TestsList actions_for_obj ;    
    /*tests_for_obj = manager__->getTestsForComponent(component, "any", 255);
    
    if(tests_for_obj.size()==0){
      return false;
    }
    else{
      return true;
    } */return true;
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if ((role != Qt::DisplayRole) && (role != Qt::DecorationRole) && (role != Qt::UserRole) && (role !=Qt::ForegroundRole)){
    	return QVariant();
    }

    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
    QVariant v = item->data();
    Component* aa = v.value<Component *>();
    //Component* aa = qVariantValue<Component *>(v); //both methods work
    if(!aa)
    {
        return item->data();
    }
    if (role == Qt::DecorationRole)
    {    //choose icon depending on component type
        if(item->getStatus()==Component::INPROGRESS)				//4
        {
            return QIcon(":images/images/wait.png");
        }

        QString pathtoicons(":icons/"); pathtoicons.append(aa->getClassName().c_str()) ;
        QDir path(pathtoicons) ;
        if ( !aa->getClassName().empty() && path.exists() )
            {
            if(item->getStatus()==Component::PASSED )  
            {
                return QIcon(pathtoicons.append("/Passed")) ;
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(pathtoicons.append("/Failed"));
            }
            else if(item->getStatus()==Component::UNRESOLVED ||  //3
                    item->getStatus()==Component::UNTESTED )       //6
            {
                return QIcon(pathtoicons.append("/Unresolved"));
            }
            return QIcon(pathtoicons.append("/Untested")) ;
            }

        switch (aa->type())
        {
        case Component::CompositeEntity:
            if(item->getStatus()==Component::PASSED )  
            {
                return QIcon(":images/images/green_composite.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_composite.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||  //3
                    item->getStatus()==Component::UNTESTED )       //6
            {
                return QIcon(":images/images/orange_composite.png");
            }
            return QIcon(":images/images/composite.png");
            break;
        case Component::Configuration:
            if(item->getStatus()==Component::PASSED)  
            {
                return QIcon(":images/images/green_composite.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_composite.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||  //3
                    item->getStatus()==Component::UNTESTED )       //6
            {
                return QIcon(":images/images/orange_composite.png");
            }
            return QIcon(":images/images/composite.png");
            break;
        case Component::Services:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_services.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_services.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_services.png");
            }
            return QIcon(":images/images/services.png");
            break;
            /*case Component::Ipc:			///not needed
                return QIcon(":images/images/ipc.png");
                break;*/
        case Component::IpcServer:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_ipc.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_ipc.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_ipc.png");
            }
            return QIcon(":images/images/ipc.png");
            break;
        case Component::IpcGeneralServer:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_ipc.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_ipc.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_ipc.png");
            }
            return QIcon(":images/images/ipc.png");
            break;
        case Component::Mrs:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_mrs_system.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_mrs_system.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_mrs_system.png");
            }
            return QIcon(":images/images/mrs_system.png");
            break;
        case Component::MrsServer:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_mrs.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_mrs.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_mrs.png");
            }
            return QIcon(":images/images/mrs.png");
            break;
        case Component::Is:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_is.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_is.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_is.png");
            }
            return QIcon(":images/images/is.png");
            break;
        case Component::IsServer:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_is.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_is.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_is.png");
            }
            return QIcon(":images/images/is.png");
            break;
        case Component::Rdb:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_rdb.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_rdb.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_rdb.png");
            }
            return QIcon(":images/images/rdb.png");
            break;
        case Component::RdbServer:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_rdb.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_rdb.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_rdb.png");
            }
            return QIcon(":images/images/rdb.png");
            break;
        case Component::CorbaServer:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_rdb.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_rdb.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_rdb.png");
            }
            return QIcon(":images/images/rdb.png");
            break;
        case Component::RmServer:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_rm.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_rm.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_rm.png");
            }
            return QIcon(":images/images/rm.png");
            break;
        case Component::Monitoring:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_monitor.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_monitor.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_monitor.png");
            }
            return QIcon(":images/images/monitor.png");
            break;
            /*case Component::RunControl:		//not used ?
                return QIcon(":images/images/rcomm.png");
                break;
            case Component::RunController:
                return QIcon(":images/images/rcomm.png");
                break;*/
        case Component::Application:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_app.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_app.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_app.png");
            }
            return QIcon(":images/images/app.png");
            break;
        case Component::Pmg:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_pmg_system.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_pmg_system.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_pmg_system.png");
            }
            return QIcon(":images/images/pmg_system.png");
            break;
        case Component::PmgAgent:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_pmg.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_pmg.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_pmg.png");
            }
            return QIcon(":images/images/pmg.png");
            break;
        case Component::Programs:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_folder.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_folder.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_folder.png");
            }
            return QIcon(":images/images/folder.png");
            break;
        case Component::Program:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_program.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_program.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_program.png");
            }
            return QIcon(":images/images/program.png");
            break;
        case Component::Hardware:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_hardware.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_hardware.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_hardware.png");
            }
            return QIcon(":images/images/hardware.png");
            break;
        case Component::Computer:
            if(item->getStatus()==Component::PASSED)
            {	// 2
                return QIcon(":images/images/green_computer.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {	// 1
                return QIcon(":images/images/red_computer.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||  //3
                    item->getStatus()==Component::UNTESTED )
            {    //6
                return QIcon(":images/images/orange_computer.png");
            }
            return QIcon(":images/images/computer.png");   //Untested 0
            break;
        case Component::VmeInterface:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_cpu.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_cpu.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_cpu.png");
            }
            return QIcon(":images/images/cpu.png");
            break;
        case Component::Interface:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_cpu.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_cpu.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_cpu.png");
            }
            return QIcon(":images/images/cpu.png");
            break;
        case Component::InputChannel:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_slink.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_slink.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED)
            {
                return QIcon(":images/images/orange_slink.png");
            }
            return QIcon(":images/images/slink.png");
            break;
            /*case Component::Link: //i think they are not used
                return QIcon(":images/images/slink.png");
                break;
            case Component::Links:
                return QIcon(":images/images/slink.png");
                break;*/
        case Component::Module:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_module.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_module.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_module.png");
            }
            return QIcon(":images/images/module.png");
            break;
        case Component::Crate:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_crate.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_crate.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_crate.png");
            }
            return QIcon(":images/images/crate.png");
            break;
        case Component::Detector:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_detector.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_detector.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_detector.png");
            }
            return QIcon(":images/images/detector.png");
            break;
            /*case Component::Network:             //// not used?
                return QIcon(":images/images/net.png");
                break;*/
        default:
            if(item->getStatus()==Component::PASSED)
            {
                return QIcon(":images/images/green_single.png");
            }
            else if(item->getStatus()==Component::FAILED)
            {
                return QIcon(":images/images/red_single.png");
            }
            else if(item->getStatus()==Component::UNRESOLVED ||
                    item->getStatus()==Component::UNTESTED )
            {
                return QIcon(":images/images/orange_single.png");
            }
            return QIcon(":images/images/single.png");
        }
    }
    //change color of the item text (DisplayRole)
    //depending on the IndividualTestResult
    if (role == Qt::ForegroundRole)
    {
    	if(item->getIndividualTestResult()==0){ //daq::tmgr::TestResult::TmPass
    		return QColor(Qt::green);
    	}
    	else if(item->getIndividualTestResult()==183){ //daq::tmgr::TestResult::TmFail
    		return QColor(Qt::red);
    	}
    	else if(item->getIndividualTestResult()==182) { //daq::tmgr::TestResult::TmUndef
    		return QColor(Qt::black);
    	}
    	else if(item->getIndividualTestResult()==184) { //daq::tmgr::TestResult::TmUNRESOLVED
    		return QColor(Qt::yellow);
    	}
    	else
    		return QColor(Qt::black);
    }
    if (role == Qt::DisplayRole)
    {    //choose text to display
        switch (aa->type())
        {
        case Component::Configuration:
            return QString((aa->name()).c_str());
            break;
        case Component::CompositeEntity:
            return QString((aa->name()).c_str());
            break;
        case Component::Services:
            return QString("Services");
            break;
        case Component::Ipc:
            return QString("IPC Servers");
            break;
        case Component::IpcServer:
            return QString("Partition IPC Server");
            break;
        case Component::IpcGeneralServer:
            return QString("Initial Partition IPC Server");
            break;
        case Component::Mrs:
            return QString("Message Reporting Services");
            break;
        case Component::MrsServer:
            return QString("MRS Server");
            break;
        case Component::Is:
            return QString("Information Services");
            break;
        case Component::IsServer:
            return QString("IS Server '").append((aa->name()).c_str()).append("'");
            break;
        case Component::Rdb:
            return QString("Databases");
            break;
        case Component::RdbServer:
            return QString((aa->name()).c_str());
            break;
        case Component::CorbaServer:
            return QString((aa->name()).c_str());
            break;
        case Component::RmServer:
            return QString((aa->name()).c_str());
            break;
        case Component::Monitoring:
            return QString("Monitoring Services");
            break;
        case Component::RunControl:		//not needed
            return QString((aa->name()).c_str());
            break;
        case Component::RunController:
            return QString((aa->name()).c_str());
            break;
        case Component::Application:
            return QString("Application '").append((aa->name()).c_str()).append("'");
            break;
        case Component::Pmg:
            return QString("Process Management");
            break;
        case Component::PmgAgent:
            return QString("PMG Server on '").append((aa->name()).c_str()).append("'");
            break;
        case Component::Programs:
            return QString("Binaries");
            break;
        case Component::Program:
            return QString("Binary '").append((aa->name()).c_str()).append("'");
            break;
        case Component::Hardware:
            return QString("Hardware");
            break;
        case Component::Computer:
            return QString("Computer '").append((aa->name()).c_str()).append("'");
            break;
        case Component::VmeInterface:
            return QString((aa->name()).c_str());
            break;
        case Component::Interface:
            return QString((aa->name()).c_str());
            break;
        case Component::InputChannel:
            return QString((aa->name()).c_str());
            break;
        case Component::Link:
            return QString((aa->name()).c_str());
            break;
        case Component::Links:
            return QString((aa->name()).c_str());
            break;
        case Component::Module:
            return QString((aa->name()).c_str());
            break;
        case Component::Crate:
            return QString((aa->name()).c_str());
            break;
        case Component::Detector:
            return QString((aa->name()).c_str());
            break;
        case Component::Network:
            return QString((aa->name()).c_str());
            break;
        default:
            return QString("No name");
        }
    }
    if (role == Qt::UserRole)
    {    //return key()
        return QVariant((aa->key()).c_str());
    }

    return QString((aa->key()).c_str());
}

//need to define the argument with all namespaces, as defined in the header. Otherwise QT does not recognise it.
//executed when "statusChanged" signal is emitted

void TreeModel::treeStatusUpdate(daq::tmgr::TestResult status, std::string comp)
{
	ERS_DEBUG(0,"Status changed to: "<<status<<" for item "<<comp);

	if(itemMap.contains(QString(comp.c_str())))
	{
		QList<TreeItem*> items2update = itemMap.values(QString(comp.c_str()));
		if (items2update.size()!=0){
			QList<TreeItem*>::iterator itemI=items2update.begin();
			for( ; itemI != items2update.end(); ++itemI) {

				//TreeItem *item = itemMap.value(QString(comp.c_str()));
				//QModelIndex index = getIndex((*itemI),rootItem);
				QModelIndex index = getIndex((*itemI),(*itemI)->parent());
				/*
				 *
				 *        enum    RetCode
				 *       {
				 *               TM_PASS        = 0,   ///<test completed and confirmed the tested functionality
 	 	 	 	 *               TM_UNDEF       = 182, ///<initial value
				 *               TM_FAIL        = 183, ///<test completed and confirmed that component is not working properly
				 *               TM_UNRESOLVED  = 184, ///<test completed but it could not verify the functionality due to internal issues
				 *               TM_UNTESTED    = 185 ///<test was not executed, e.g. some problems with test manager itself or with test configuration
				 *       };
				 */
				daq::dvs::Component::Status component_status;
				switch (status)
				{
				case daq::tmgr::TestResult::TmPass:
					component_status=daq::dvs::Component::Status::PASSED;
					break;
				case daq::tmgr::TestResult::TmUndef:
					component_status=daq::dvs::Component::Status::UNDEFINED;
					break;
				case daq::tmgr::TestResult::TmFail:
					component_status=daq::dvs::Component::Status::FAILED;
					break;
				case daq::tmgr::TestResult::TmUnresolved:
					component_status=daq::dvs::Component::Status::UNRESOLVED;
					break;
				case daq::tmgr::TestResult::TmUntested:
					component_status=daq::dvs::Component::Status::UNTESTED;
					break;
				default:
					component_status=daq::dvs::Component::Status::UNRESOLVED;
				}
				(*itemI)->setStatus(component_status);

				// emit dataChanged(index, index);
				if(status != daq::tmgr::TestResult::TmUndef)
					emit testingCompleted(comp);
				else
					emit resetDone("Reset");

                if (status == daq::tmgr::TestResult::TmFail)
                    {
                    emit failedComponent(index) ; 
                    }
			}
		}
	}
	else
	{
		ERS_DEBUG(0,"Item not found");
	}

}

//executed when signal "messageChanged" is emitted
void TreeModel::treeItemChanged(std::string comp_id)
{
	if(itemMap.contains(QString(comp_id.c_str())))
	{
		QList<TreeItem*> items2update = itemMap.values(QString(comp_id.c_str()));
		if (items2update.size()!=0){
			QList<TreeItem*>::iterator itemI=items2update.begin();
			for( ; itemI != items2update.end(); ++itemI) {
				QVariant v = (*itemI)->data();
				Component* aa = v.value<Component *>();

				std::string msg="";std::string msg_out="";std::string msg_err="";uint32_t indivResult=0;
				indivResult=aa->getResult().componentResult.result;
				//std::cout<<"INDIVIDUAL result: "<<indivResult<<std::endl;
				(*itemI)->setIndividualTestResult(indivResult);

				//std::time_t t = system_clock::to_time_t(aa->getResult().componentResult.test_time);
				//char buff [30];std::string ttstr="";
				//std::strftime(buff, 30, "[%T] ",std::localtime(&t));

				//std::cout << "TEST TIME: "<< buff << std::endl;
				//std::string ttstr = std::ctime(&t);
				//ttstr.resize(ttstr.size()-1);
				
                //ttstr=buff;
				
                /*ERS_INFO(aa->key()<<" GlobalResult: "<<aa->getResult().globalResult<<"; IndividualResult: "<<indivResult);
				std::stringstream bb;
				bb << aa->getResult().globalResult;
				std::string bbstr;
				bb >> bbstr;
				std::stringstream cc;
				cc << indivResult;
				std::string ccstr;
				cc >> ccstr;
				msg_out+="GlobalResult: "+bbstr+"; IndividualResult: "+ccstr+"</b>\n";*/
                std::string diagnosis, actions ;
				for (auto& tr: aa->getResult().componentResult.test_results){

                    std::stringstream time ;
                    std::time_t tt = system_clock::to_time_t(tr.test_time) ;
                    struct std::tm * ptm = std::localtime(&tt);
                    time << std::put_time(ptm, "[%T] ") ;
					
                    std::stringstream ss;
					ss << tr.return_code;

					msg_out+="\n"+time.str() +" <b>"+tr.test_id+"</b> returned <b>"+resultStr[tr.return_code]+"</b> (<b>"+ ss.str() +"</b>)\n";
					msg_err+="\n"+time.str() +" <b>"+tr.test_id+"</b> returned <b>"+resultStr[tr.return_code]+"</b> (<b>"+ ss.str() +"</b>)\n";
					msg_out+=tr.stdout;
					msg_err+=tr.stderr;

                    if ( !tr.diagnosis.empty() ) 
                        {
                        diagnosis.append(tr.diagnosis).append("\n") ;
                        }
                    }

                for (const auto& aa_act: aa->getResult().actions)
                    {
                    actions.append("command:\t").append(aa_act.command) ;
                    actions.append("\nparameters:\t").append(aa_act.parameters_json).append("\n") ;
                    }

				(*itemI)->setTest_out(msg_out);
				(*itemI)->setTest_err(msg_err);

				(*itemI)->setDiagnosis(diagnosis);
                (*itemI)->setReason(actions);
/*
				std::string act="";
				for (const auto& aa_act: aa->getTestResult().actions){
					act+=aa_act->getDiagnosis();
					{
					const std::shared_ptr<daq::tm::RebootAction> RAction = std::dynamic_pointer_cast<daq::tm::RebootAction>(aa_act);
					if(RAction.get() != nullptr && &RAction->getHost() !=nullptr){// .class_name()=="Computer"){
						act+=" Reboot ["+RAction->getHost().UID()+"].";
					}
					else {
						act+="\n";
					}}
					{
					const std::shared_ptr<daq::tm::ExecAction> ExecAction = std::dynamic_pointer_cast<daq::tm::ExecAction>(aa_act);
					if(ExecAction.get()!=nullptr) {
						act+=" List of executables: ";
						for( const auto& executables: ExecAction->getExecutables() ){
							act+="\ncmd line: "+executables.cmdLinePars;
							std::cout<<"cmd line: "<<executables.cmdLinePars<<std::endl;
							if(executables.host !=nullptr){
							act+="\nHost: "+executables.host->UID();std::cout<<"host: "<<executables.host->UID()<<std::endl;}
							else {act+="\nNo host";std::cout<<"No host"<<std::endl;}
							if(executables.program !=nullptr) {
							act+="\nBinary: "+executables.program->UID();std::cout<<"Binary: "<<executables.program->UID()<<std::endl;}
							else {act+="\nNo binary";std::cout<<"No binary"<<std::endl;}

							act+="\nInitTimeout: "+executables.initTimeout;std::cout<<"InitTimeout: "<<executables.initTimeout<<std::endl;

							//act+="\nTimeout: "+executables.timeout; std::cout<<"Timeout: "<<executables.timeout<<std::endl;

							for( const auto& exec_vect: executables.executables ) {
								act+="\nExecutables: "+exec_vect;
								std::cout<<"Executables: "<<exec_vect<<std::endl;
							}
						}
					}
					else {
						act+="\n";
					}}
					{
					const std::shared_ptr<daq::tm::TestApplicationAction> TUFappaction = std::dynamic_pointer_cast<daq::tm::TestApplicationAction>(aa_act);
					if(TUFappaction.get()!=nullptr) {
						act+="Test Application: "+TUFappaction->getApplication()->UID();
					}
					else {
						act+="\n";
					}}
					{
					const std::shared_ptr<daq::tm::TestObjectAction> TUFobjaction = std::dynamic_pointer_cast<daq::tm::TestObjectAction>(aa_act);
					if(TUFobjaction.get()!=nullptr) {
						act+="Test Testable object: "+TUFobjaction->getTestableObject().UID();
					}
					else {
						act+="\n";
					}}

				}
				(*itemI)->setReason(act);
                */
			}
		}
	emit itemUpdated(comp_id);
	}
	else
	{
		ERS_DEBUG(0,"Item not found");
	}
}

QModelIndexList TreeModel::match(const QModelIndex &start, int role,const QString &name, int hits,
                                 Qt::MatchFlags flags) const
{

    hits =1;
    QModelIndexList result;
    QModelIndex p = parent(start);

    QVariant value(name);
    int from = start.row();
    int to = rowCount(p);
    for (int r = from; (r < to) && (result.count() < 1); ++r)
    {
        QModelIndex idx = index(r, start.column(), p);
        if (!idx.isValid())
            continue;
        QVariant v = data(idx, role);
        if (value == v)
        {
            result.append(idx);
        }
        TreeItem* node= static_cast<TreeItem*>(idx.internalPointer());
        //std::cout<< "Key of node found is " << node->getKey() <<std::endl;
        if (node->childCount() != 0)
        { // search the hierarchy
            result += match(index(0, idx.column(), idx), role, name, 1, flags);
        }
    }
    return result;
}

int TreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}


Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;    
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        return rootItem->data();
    }
    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent) const
{
    TreeItem *parentItem;

    if (!parent.isValid())
    {
        parentItem = rootItem;
    }
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
    {
        //std::cout<<"childItem at row :"<<row<<" is: "<<childItem->getKey()<<std::endl;
        return createIndex(row, column, childItem);
    }
    else
    {
        //std::cout<<"No child item at row "<<row<<std::endl;
        return QModelIndex();
    }
}

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parent();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}

QModelIndex TreeModel::getIndex( TreeItem* item,TreeItem* parent)
{
	for(int i=0; i<parent->childCount();++i)
    {
        if(item == parent->child(i))
        {
        	return createIndex(i, 0,  parent->child(i));
        }

        if(getIndex(item,parent->child(i)).isValid()){
        	return getIndex(item,parent->child(i));

        }

    }
	return QModelIndex();
}

