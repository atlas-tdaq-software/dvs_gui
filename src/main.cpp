#include <QApplication>
#include <QResource>
#include <QDir>
#include <dvs_gui/dvs_mainwindow.h>
#include <cmdl/cmdargs.h>

using namespace dvs_gui;

constexpr const char* user_icons_file_env = "TDAQ_DVS_USER_ICONS" ;
constexpr const char* user_icons_filename = "/.tdaq/dvs/icons.rcc" ;

int main(int argc, char *argv[])
{
    CmdArgStr database('d', "database", "database-name", "configuration database to be used.", CmdArg::isREQ);
    CmdArgStr partition('p', "partition", "partition", "selected partition ", CmdArg::isREQ);
    CmdArgStr user_resources('r', "resource", "resource", ".rcc file with user defined icons, overrides TDAQ_DVS_USER_ICONS", CmdArg::isOPT);

    // Declare command object and its argument-iterator
    CmdLine  cmd(*argv, &database, &partition, &user_resources, NULL);
    CmdArgvIter	 arg_iter(--argc, ++argv);
    // Parse arguments
    cmd.parse(arg_iter);

    QStringList paths = QCoreApplication::libraryPaths();
    char* env = getenv("QT_PLUGINS");
    if(env)
    {
        paths << QString(env);
        QCoreApplication::setLibraryPaths(paths);
    }

    QApplication::setStyle("fusion");
    QApplication app(argc, argv);

    const char* usericonsfile = nullptr ;
    if ( user_resources.flags() && CmdArg::GIVEN ) {
        usericonsfile = (const char*)user_resources ;
    }
    else
    if ( getenv(user_icons_file_env) != nullptr ) {
        usericonsfile = getenv(user_icons_file_env) ;
    }
    else {
        QString filename = QDir::homePath() ; filename.append(user_icons_filename) ;
        if ( QFile(filename).exists() ) {
            usericonsfile = filename.toLocal8Bit().data() ;
        }
    }
    
    if ( usericonsfile != nullptr ) {
    if ( !QResource::registerResource((const char*)usericonsfile) ){
        std::cerr << "Failed to load user Qt resources fom .rcc file " << user_resources << std::endl ;
        exit(1) ;
        }
    else {
        std::cerr << "Registered user icons as Qt resource from " << usericonsfile << std::endl ;
    } }
    else {
        std::cerr << "User icons not configured" << std::endl ;
    }
    
    DVSMainWindow *frame = new DVSMainWindow(0,(const char*)database, (const char*)partition);
    frame->show();

    return app.exec();
}
