 /****************************************************************************
 **
 ** Copyright (C) 2005-2008 Trolltech ASA. All rights reserved.
 **
 ** This file is part of the documentation of the Qt Toolkit.
 **
 ****************************************************************************/

 /*
     tree_item.cpp

     A container for items of data supplied by the simple tree model.
 */

 #include <QStringList>

 #include <dvs_gui/tree_item.h>

 using namespace dvs_gui;

 TreeItem::TreeItem(const QVariant &data, const std::string & key, TreeItem *parent)
 {
     parentItem = parent;
     itemData = data;
     key_=key;
     result_="";
     reason_="";
     diagnosis_="";
     indivResult_=182;	//TmUndef
     status_=daq::dvs::Component::UNDEFINED;
 }

 TreeItem::~TreeItem()
 {
     qDeleteAll(childItems);
 }

 void TreeItem::appendChild(TreeItem *item)
 {
     childItems.append(item);
 }

 TreeItem* TreeItem::child(int row)
 {
     return childItems.value(row);
 }

 int TreeItem::childCount() const
 {
     return childItems.count();
 }

 int TreeItem::columnCount() const
 {
     return 1;
 }

 QVariant TreeItem::data() const
 {
     return itemData;
 }

 TreeItem* TreeItem::parent()
 {
     return parentItem;
 }

 int TreeItem::row() const
 {
     if (parentItem)
         return parentItem->childItems.indexOf(const_cast<TreeItem*>(this));

     return 0;
 }

 std::string TreeItem::getTest_out() { return test_out; }

 void TreeItem::setTest_out(std::string r)
 {
   test_out=r;
//   std::cout<<"result_ is "<<result_<<std::endl;
 }
 std::string TreeItem::getTest_err() { return test_err; }

 void TreeItem::setTest_err(std::string r)
 {
   test_err=r;
//   std::cout<<"result_ is "<<result_<<std::endl;
 }

 std::string TreeItem::getDiagnosis() { return diagnosis_; }

 void TreeItem::setDiagnosis(std::string d)
 {
    diagnosis_ = d;
 }

 std::string TreeItem::getReason() { return reason_; }
 void TreeItem::setReason(std::string re)
 {
    reason_ ="\t";
    reason_ = re;
 }
