 #include <QtGui>
 #include <dvs_gui/single_testing_thread.h>
 #include <dvs/dvscomponent.h>
 #include <dvs_gui/signals.h>

 using namespace daq::dvs;
 using namespace dvs_gui ;

 SingleTestingThread * SingleTestingThread::instance_ = NULL;

 SingleTestingThread::SingleTestingThread(QObject *parent)
     : QThread(parent)
 {ERS_DEBUG(0, "single test thread created.");
     restart = false;
     abort = false;
     instance_=this;
     gs=new GUISignals();
 }

 SingleTestingThread::~SingleTestingThread()
 {ERS_DEBUG(0, "single test thread destroyed.");
     mutex.lock();
     abort = true;
     condition.wakeOne();
     mutex.unlock();
     wait();
     delete gs;
     delete instance_;
 }

 void SingleTestingThread::getTestingParams(Manager* manager, daq::dvs::Component* comp, QString test_id, std::string scope, int level)
 {
     ERS_INFO("start single test for component "<<comp->key());
     QMutexLocker locker(&mutex);

     this->testID = test_id;
     this->testing_comp = comp;
     this->manager = manager;
     this->scope=scope;
     this->level=level;

     if (!isRunning()) {
         start(LowPriority);
     } else {
         restart = true;
         condition.wakeOne();
     }
 }


 void SingleTestingThread::run()
 {
     forever {
         mutex.lock();
         QString testID__ = this->testID;
         daq::dvs::Component* testing_comp__ = this->testing_comp;
         Manager* manager__ = this->manager;
	     std::string scope__=this->scope;
	     int level__=this->level;
         mutex.unlock();

         if(testing_comp__){
             ERS_INFO("START single test");
        	 daq::dvs::Result result =manager__->testComponent(testing_comp__,testID__.toStdString(),scope__,level__);
        	 ERS_DEBUG(0, "DVS callback received for " << result.componentId << " daq::dvs::Result.testResult.result: " << result.componentResult.result);
             SingleTestingThread::instance_->gs->emitterStatusChanged(result.globalResult,result.componentId);
             SingleTestingThread::instance_->gs->emitterMessageChanged(result.componentId);
        }

         if (restart)
              break;
         if (abort)
              return;

         mutex.lock();
         if (!restart)
            condition.wait(&mutex);
         restart = false;
         mutex.unlock();
     }
 }
