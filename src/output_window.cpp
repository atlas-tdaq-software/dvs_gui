/*
 * output_window.cpp
 *
 *  Created on: Feb 2, 2009
 *      Author: alina
 */
#include <QtGui>
#include <QtWidgets>
#include <dvs_gui/output_window.h>
#include <iostream>
class QFileSystemWatcher;

OutputWindow::OutputWindow(QWidget *parent, std::string part)
	: QWidget(parent),
        partition_name(part)
{
    textEdit = new QTextEdit;
    textEdit->setReadOnly(true);

    closeButton = new QPushButton(tr("&Close"));
    connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(textEdit);
    layout->addWidget(closeButton);
    setLayout(layout);

    setWindowTitle(tr("DVS Output Window"));
    QWidget::setWindowFlags(Qt::Window);

    QString out=getDVSOutputFile();
    textEdit->setPlainText(out);
    std::string logname;
    char* env = getenv("TDAQ_LOGS_PATH");
    if(env)
        logname = std::string(env) + "/dvs.log";
    else    
        logname="/tmp/"+partition_name+"/dvs.log";
    QFile *logFile = new QFile(QString(logname.c_str()));
    new QTextStream(logFile); 
    QFileSystemWatcher *fileWatcher = new QFileSystemWatcher(this) ;
    fileWatcher->addPath (QString(logname.c_str()));
    //QObject::connect(fileWatcher, SIGNAL(fileChanged(QString)), this, SLOT(readContents(QString))) ; 
    QObject::connect(fileWatcher, SIGNAL(fileChanged(QString)), this, SLOT(printContent())) ; 
}

void OutputWindow::printContent()
{
    QString out=getDVSOutputFile();
    textEdit->setPlainText(out);

}

QString OutputWindow::getDVSOutputFile()
{
    std::string cmdlog;
    char* env = getenv("TDAQ_LOGS_PATH");
    if(env)
        cmdlog = "cat " + std::string(env) + "/dvs.log";
    else    
	cmdlog = "cat /tmp/" + partition_name + "/dvs.log";
    QString out;
    QProcess *getlog = new QProcess();
    getlog->setReadChannelMode(QProcess::MergedChannels);
    QApplication::setOverrideCursor(Qt::WaitCursor);

    getlog->start("sh",QStringList() << "-c"<<QString(cmdlog.c_str()));
    if(!getlog->waitForStarted(5000))
    {
        getlog->kill();
        QApplication::restoreOverrideCursor();
        return "";

    }
    if (!getlog->waitForFinished(10000))//10 second timeout to finish
    {
        getlog->terminate();
        if (!getlog->waitForFinished(2000))//2 second timeout to terminate
        {
            getlog->kill();
        }
     }
    else
    {
        out = "Reading file with command: "+QString(cmdlog.c_str());
        out+="\n---------------------------------------------------------------------\n";
        out += getlog->readAll();
    }
    QApplication::restoreOverrideCursor();
    return out;
}

OutputWindow::~OutputWindow()
{
}
