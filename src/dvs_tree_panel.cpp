//***********
//dvs_tree_panel.cpp implementation of the dvs tree widget
//*************//
#include <QtGui> 

#include <dvs_gui/dvs_tree_panel.h>
//#include <dvs_gui/tree_model_proxy.h>
#include <dvs_gui/tree_model.h>
#include <ers/ers.h>   
#include <dvs_gui/dvs_mainwindow.h>

using namespace dvs_gui;

DVSTreePanel::DVSTreePanel(QWidget* parent)
     : QWidget(parent)
{
   setupUi(this);

}

void DVSTreePanel::loadDVSComponentTree(std::shared_ptr<daq::dvs::Component> thetree, const char* partition, daq::dvs::Manager* manager)
{

   if(!thetree) {
      ERS_LOG("No valid tree");
      exit(0);
   }
   model = new TreeModel(thetree, partition, manager);
   treeView = new MyTreeView(this);
   treeView->setModel(model);
   treeView->expand(model->index(0,0));

   //TreeModelProxy *proxyModel = dynamic_cast<TreeModelProxy *>( model );
   //proxyModel = new TreeModelProxy();
   //proxyModel->setSourceModel(model);

   //proxyModel->setSortCaseSensitivity(Qt::CaseInsensitive);
   //proxyModel->sort(0, Qt::AscendingOrder);
   //treeView->setModel(proxyModel);

   /*proxyModel = new QSortFilterProxyModel;
   proxyModel->setDynamicSortFilter(true);
   proxyModel->setSourceModel(model);
   proxyModel->setSortCaseSensitivity(Qt::CaseInsensitive);
   proxyModel->sort(0, Qt::AscendingOrder);

   treeView->setModel(proxyModel);
   */
   layout = new QGridLayout(this);   
   layout->addWidget(treeView,0,0);
   setLayout(layout);
}

void DVSTreePanel::cleanup()
{
   if (model) {      
      delete model;
      //model=0;
      ERS_DEBUG(1,"Delete existing model");
   }
   if (layout) {
      delete layout;
      //layout=0;
      ERS_DEBUG(1,"Delete existing layout");
   }
   if (treeView) {
      delete treeView;
      //treeView=0;
      ERS_DEBUG(1,"Delete existing view");
   }
}
   
