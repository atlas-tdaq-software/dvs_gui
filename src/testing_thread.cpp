 #include <QtGui>

 #include <dvs_gui/testing_thread.h>
 #include <dvs/dvscomponent.h>
 #include <dvs_gui/signals.h>

 using namespace daq::dvs;
 using namespace dvs_gui ;

 TestingThread * TestingThread::instance_ = NULL;

 void boum() { std::cout << "Thread run: BOUM!" << std::endl; }
 void TestCallback(daq::dvs::Result answer)
 {
 ERS_DEBUG(0, "DVS callback received for " << answer.componentId);
         // std::cerr << answer.componentResult.component_id << " tested:" << answer.componentResult.result << std::endl ;
         /*for ( auto res: answer.componentResult.testResults )
                 {
                 std::cerr << "\t" << res.testID << ": " << res.returnCode << std::endl ;
                 std::cerr << "\t## STD OUT #########" << res.stdout << std::endl ;
                 std::cerr << "\t## STD ERR #########" << res.stderr << std::endl ;
                 }*/
         TestingThread::instance_->gs->emitterStatusChanged(answer.globalResult,answer.componentId);
         TestingThread::instance_->gs->emitterMessageChanged(answer.componentId);
 }

 TestingThread::TestingThread(QObject *parent)
     : QThread(parent)
 {
	 ERS_DEBUG(0, "test thread created.");
	 restart = false;
     abort = false;
     instance_=this;
     gs=new GUISignals();
 }

 TestingThread::~TestingThread()
 {ERS_DEBUG(0, "test thread destroyed.");
     mutex.lock();
     abort = true;
     condition.wakeOne();
     mutex.unlock();
     delete gs;
     delete instance_;
     wait();
 }

 void TestingThread::getTestingParams(Manager* manager, daq::dvs::Component* comp, std::string scope, int level)
 {
     QMutexLocker locker(&mutex);

     this->testing_comp = comp;
     this->manager = manager;
     this->scope=scope;
     this->level=level;

     if (!isRunning()) {
         start(LowPriority);
     } else {
         restart = true;
         condition.wakeOne();
     }
 }

 void TestingThread::run()
 {
     forever {
         mutex.lock();
         daq::dvs::Component* testing_comp__ = this->testing_comp;
	     Manager* manager__ = this->manager;
	     std::string scope__=this->scope;
	     int level__=this->level;

         mutex.unlock();
         if(testing_comp__){

        	 //std::function< void() > callback=&boum;
        	 std::function< void(daq::dvs::Result) > callback=&TestCallback;
        	 std::future<daq::dvs::Result> futures = manager__->testComponent(testing_comp__,(daq::dvs::Callback)&TestCallback ,scope__, level__);
         }

         if (restart)
              break;
         if (abort)
              return;

         mutex.lock();
         if (!restart)
            condition.wait(&mutex);
         restart = false;
         mutex.unlock();
     }
 }
