#include <QtGui>
#include <QtWidgets>
#include <ers/ers.h>
#include <dvs_gui/level_scope_selector_dialog.h>

using namespace dvs_gui;

LevelScopeSelector::LevelScopeSelector(QWidget* parent,int prev_level, QString prev_scope)
     : QDialog(parent)
{

   QLabel* label = new QLabel(tr("Please select a complexity level and the scope of a testing session.\n\nAll tests with complexity level up to the given value are executed."
		   "\n\nScope defines the context in which the test execution is allowed. \nTests with scope 'any' defined are always executed. "));

   comboBox_levelsel = new QComboBox(this);
   QStringList items;
   items << tr("all")<< tr("0") << tr("1") << tr("2") << tr("3");
   comboBox_levelsel->addItems(items);
   if(prev_level==255)
	   comboBox_levelsel->setCurrentIndex(0);
   else
	   comboBox_levelsel->setCurrentIndex(prev_level+1);

   comboBox_scope = new QComboBox(this);
   QStringList it;
   //TO DO: fill this combo with values from the db
   it << tr("any") << tr("precondition") << tr("functional") << tr("diagnostics");
   comboBox_scope->addItems(it);
   int index = comboBox_scope->findText(prev_scope);
   if ( index != -1 ) {
      comboBox_scope->setCurrentIndex(index);
   }

   buttonBox = new QDialogButtonBox(QDialogButtonBox::Cancel | QDialogButtonBox::Ok, Qt::Horizontal);
   QGridLayout *mainLayout = new QGridLayout;
   mainLayout->setSizeConstraint(QLayout::SetFixedSize);
   mainLayout->addWidget(label, 0, 0);
   mainLayout->addWidget(comboBox_levelsel, 1, 0);
   mainLayout->addWidget(comboBox_scope, 1, 1);
   mainLayout->addWidget(buttonBox, 2, 0);
   setLayout(mainLayout);

   setWindowTitle(tr("Level and Scope"));
   connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
   connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}
