 #include <QtGui>

 #include <dvs_gui/reset_state_thread.h>
 
 using namespace daq::dvs;
 using namespace dvs_gui ;

 ResetStateThread::ResetStateThread(QObject *parent)
     : QThread(parent)
 {
     restart = false;
     abort = false;
 }

 ResetStateThread::~ResetStateThread()
 {
     mutex.lock();
     abort = true;
     condition.wakeOne();
     mutex.unlock();

     wait();
 }

 void ResetStateThread::getResetParams(Manager* manager, daq::dvs::Component* comp)
 {
     QMutexLocker locker(&mutex);

     this->reset_comp = comp;
     this->manager = manager;

     if (!isRunning()) {
         start(LowPriority);
     } else {
         restart = true;
         condition.wakeOne();
     }
 }

 void ResetStateThread::run()
 {
     forever {
         mutex.lock();
         Component* reset_comp__ = this->reset_comp;
	     Manager* manager__ = this->manager;
         mutex.unlock();

         manager__->resetComponent(reset_comp__);

         if (restart)
              break;
         if (abort)
              return;

         mutex.lock();
         if (!restart)
            condition.wait(&mutex);
         restart = false;
         mutex.unlock();
     }
 }
