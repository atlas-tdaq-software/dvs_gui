#include <QtGui> 
#include <QContextMenuEvent>
#include <ers/ers.h>   
#include <dvs_gui/my_treeview.h>   

using namespace dvs_gui;

MyTreeView::MyTreeView(QWidget* parent)
     : QTreeView(parent)
{
     setExpandsOnDoubleClick(false) ;
}

void MyTreeView::expandSubtree(const QModelIndex& index) 
{
     if (!isExpanded(index)) {
        expandAllChildren(index, true) ;
     }
     else {
        expandAllChildren(index, false) ;
     }
}
 
void MyTreeView::expandAllChildren(const QModelIndex &index, bool flag)
{
    if (!index.isValid()) {
        std::cerr << "index not valid" << std::endl ;
        return;
    }

    for (int i = 0; i < index.model()->rowCount(index); i++) {
        expandAllChildren(index.child(i, 0), flag);
    }

    setExpanded(index,flag) ;
}

// make a Failed component visible (signal emitted from )
// recursively expand all parents
void MyTreeView::expandToFailed(const QModelIndex& index) 
{
     QModelIndex parent = index ;
     while ( parent.isValid() )
          {
          setExpanded(parent, true) ;
          parent = parent.parent() ;
          }
}

void MyTreeView::contextMenuEvent(QContextMenuEvent *event)
{
     emit giveMePosition( event->globalPos() );
}
