//***********
//
//*************//
#include <dvs_gui/signals.h>

using namespace daq::dvs ;
using namespace dvs_gui ;


void GUISignals::emitterStatusChanged( daq::tmgr::TestResult status, std::string comp_id )
{

	ERS_DEBUG(0,"** Component: "<<comp_id<<" changed status after testing: "<<status);

	//call DVSTreePanel to change the icon colors and the status of the component.
	emit statusChanged(status,comp_id);

}

void GUISignals::emitterMessageChanged( std::string comp_id )
{
	ERS_DEBUG(0,"** Component: "<<comp_id<<" changed output");

	emit messageChanged(comp_id);

}


