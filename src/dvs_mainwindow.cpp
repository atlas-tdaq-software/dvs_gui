//***********
//dvs_mainwindow.cpp implementation of the dvs main widget
//*************//

#include <iostream>
#include <QtGui>
#include <QtWidgets>
#include <QMetaType>
#include <QTUtils/highlighter.h>
#include <QDesktopServices>
#include <QUrl>

#include <ers/ers.h>
#include <ipc/core.h>
#include <ProcessManager/client_simple.h>
#include <dvs/dvsmanager.h>
#include <dvs_gui/tree_item.h>
#include <dvs_gui/level_scope_selector_dialog.h>
#include <dvs_gui/output_window.h>
#include <dvs_gui/help_browser.h>
#include <dvs_gui/dvs_mainwindow.h>


using namespace daq::dvs;
using namespace dvs_gui;

//declare the type
Q_DECLARE_METATYPE(daq::dvs::Component::Status)
Q_DECLARE_METATYPE(std::string)
Q_DECLARE_METATYPE(daq::dvs::Component::Type)
Q_DECLARE_METATYPE(daq::dvs::Component*)
Q_DECLARE_METATYPE(QTextCursor)
Q_DECLARE_METATYPE(daq::tmgr::TestResult)

Configuration * DVSMainWindow::config = NULL;

DVSMainWindow::DVSMainWindow(QWidget* parent, const char* db_name, const char* partition_name)
        : QMainWindow(parent),
        db_name_(db_name),
        partition_name_(partition_name)
{
    setupUi(this);

    std::list< std::pair<std::string, std::string> > alist ;
    try {
        IPCCore::init(alist) ;
        daq::pmg::Singleton::init() ;
    }
    catch ( daq::ipc::Exception & ex ) {
        ers::error( ex ) ;
    }
    catch ( daq::pmg::Exception & ex ) {
        ers::error( ex ) ;
    } ;


    //QString title = windowTitle();
    QString parttitle="<< ";
    parttitle.append(partition_name_).append(" >>	");
    this->setWindowTitle(windowTitle().prepend(parttitle));

    //register the type needed for slot/signal connection
    qRegisterMetaType<daq::dvs::Component::Status>();
    qRegisterMetaType<std::string>();
    qRegisterMetaType<QTextCursor>();
    qRegisterMetaType<daq::tmgr::TestResult>();

    connect(actionTest_Component, SIGNAL(triggered()), this, SLOT(startTest()) );
    connect(actionStop_testing, SIGNAL(triggered()), this, SLOT(stopTest()) );
    connect(actionReload_configuration, SIGNAL(triggered()), this, SLOT(reloadDB()) );
    connect(actionEdit_configuration, SIGNAL(triggered()), this, SLOT(editDB()) );
    connect(actionReset_Component, SIGNAL(triggered()), this, SLOT(resetState()) );
    connect(tabWidget, SIGNAL(currentChanged(int )), this, SLOT(updateTabs(int )) );
    connect(tabWidget_logview, SIGNAL(currentChanged(int )), this, SLOT(updateTabs(int )) );
    connect(actionTest_level_scope_selector, SIGNAL(triggered()), this, SLOT(changeLevelScope()) );
    connect(findNextButton_2, SIGNAL(clicked()), this, SLOT(findNext()) );
    connect(findPreviousButton_2, SIGNAL(clicked()), this, SLOT(findPrevious()) );
    connect(searchText_2, SIGNAL(textChanged(const QString & )), this, SLOT(findFirst(const QString & )) );
    connect(actionLog_viewer_settings, SIGNAL(triggered()), this, SLOT(logViewerSettings()));

    setenv("TDAQ_DB_DATA",db_name,true);
    manager = new Manager();

    test_level=255;  //set the initial complexity value
    test_scope="any";
    log_set=0; //set the initial default value to the first option
    view_cmd =" cat `ls -t ";
    end_view_cmd=" | head -1`";
    fileName="";
    combineChildrenOutput=false;
    HWview=false;

    if (getenv("TDAQ_SETUP_POINT1")!=NULL && atoi(getenv("TDAQ_SETUP_POINT1"))==1)
    {
        ERS_LOG("Run in P1");
        run_P1_=true;
        this->actionEdit_configuration->setDisabled(true);
    }
    else
    {
        ERS_LOG("Outside P1 env.");
        run_P1_=false;
    }

    try
    {
        config = new Configuration(std::string("oksconfig:")+db_name_);

    }
    catch( daq::config::Exception & ex )
    {
        ers::error( ex );
        exit(1);
    }
        
    setupHighlighters();

    loadDB();

    subscribeToDatabaseChanges();

    connect(&dbcallback_thread, SIGNAL(finish_notify()), this, SLOT(reloadDB()));
    
    connect(thread.instance_->gs, SIGNAL(statusChanged(daq::tmgr::TestResult, std::string )), DVSTreeView->model, SLOT(treeStatusUpdate(daq::tmgr::TestResult, std::string )) );
    connect(thread.instance_->gs, SIGNAL(messageChanged(std::string )), DVSTreeView->model, SLOT(treeItemChanged(std::string  )) );
    connect(DVSTreeView->model, SIGNAL(itemUpdated(std::string )), this, SLOT(updateTabs(std::string )));

    connect(DVSTreeView->treeView->selectionModel(), SIGNAL(currentChanged(const QModelIndex &, const QModelIndex &)),this, SLOT(printInTab(const QModelIndex &))); 

    connect(DVSTreeView->model, SIGNAL(failedComponent(const QModelIndex &)), DVSTreeView->treeView, SLOT(expandToFailed(const QModelIndex&))) ;
//    connect(DVSTreeView->treeView, SIGNAL(entered(const QModelIndex &)), this, SLOT(displayItemInfo(const QModelIndex & )));

    //connect(DVSTreeView->treeView, SIGNAL(doubleClicked(const QModelIndex &)), DVSTreeView->treeView, SLOT(expandRecursively(const QModelIndex&))) ;
    // TODO from 5.13 expandRecursively is available
    //connect(DVSTreeView->treeView, SIGNAL(doubleClicked(const QModelIndex &)), DVSTreeView->treeView, SLOT(expandSubtree(const QModelIndex&))) ;
    connect(DVSTreeView->treeView, SIGNAL(activated(const QModelIndex &)), DVSTreeView->treeView, SLOT(expandSubtree(const QModelIndex&))) ;

    connect(DVSTreeView->treeView, SIGNAL(giveMePosition(const QPoint & )), this, SLOT(showTests(const QPoint &)) );
    connect(highlightAll_2,SIGNAL(clicked()),this,SLOT(highlightSearch()));
    connect(DVSTreeView->model, SIGNAL(testingCompleted(std::string )), this, SLOT(enableButtons(std::string )) );
    connect(DVSTreeView->model, SIGNAL(resetDone(std::string )), this, SLOT(enableButtons(std::string )) );

    connect(s_thread.instance_->gs, SIGNAL(statusChanged(daq::tmgr::TestResult, std::string )), DVSTreeView->model, SLOT(treeStatusUpdate(daq::tmgr::TestResult, std::string )) );
    connect(s_thread.instance_->gs, SIGNAL(messageChanged(std::string )), DVSTreeView->model, SLOT(treeItemChanged(std::string  )) );
    
    
}

DVSMainWindow::~DVSMainWindow()
{
    ERS_DEBUG(1,"Deleting main window");
    if(manager)
    {
        delete manager;
        manager =0;
    }
}

void DVSMainWindow::closeEvent(QCloseEvent *event)
{
    ERS_DEBUG(0,"Closing");
    if(manager)
    {
        delete manager;
        manager =0;
    }
    event->accept();//continue with closing main application
    //event->ignore();//don't accept to close the window
}

void DVSMainWindow::loadDB()
{
    if (!config->loaded())
    {
        ers::error( ers::Message( ERS_HERE,"Database is not correctly loaded")) ;
    }
    ERS_LOG("DB: "<<db_name_<<" loaded OK.");

    try
    {
	//std::shared_ptr<Component> loadConfiguration(Configuration& configuration, const std::string& partition, const std::string& segmentID = "", bool recursive = true, bool skip_apps = false) ;
        tree = manager->loadConfiguration(*config,std::string(partition_name_),"",true,HWview) ;
        logroot_ = manager->get_PartitionLogRoot(); //check if is reloaded if changed
        initlogroot_ = manager->get_InitialLogRoot();
    }
    catch ( CannotLoadDatabase & ex )
    {
        ers::error(ex) ;
        exit(1);
    }

    if ( tree )
    {
        try {
        	DVSTreeView->loadDVSComponentTree(tree,partition_name_,manager);
        }
        catch ( CannotLoadDatabase & ex )
        {
            ers::error(ex) ;
            exit(1);
        }
    }
    else
        ERS_DEBUG(1,"(empty tree)");
}

void DVSMainWindow::subscribeToDatabaseChanges()
{
    ERS_DEBUG(0, "Registering database callback");
    if (!config)
	return;

    try {
	//start the thread that is doing the subscription to db changes
        dbcallback_thread.getSubscriptionParams(config);
    }
    catch(daq::config::Exception& e) {
       ers::warning(ers::Message(ERS_HERE,"Failed to subscribe on database changes"));
    }
    ERS_DEBUG(0, "Subscription OK");
}

void DVSMainWindow::reloadDB(bool HWView)
{
    ERS_DEBUG(0,"Reload the configuration.");
    this->setCursor(Qt::WaitCursor);

    try
    {
        tree = manager->reloadConfiguration(HWView);	
    }
    catch ( CannotLoadDatabase & ex )
    {
        ers::error(ex) ;
        exit(1);
    }
    catch ( daq::config::Generic & ex )
    {
        ers::error( ex ) ;
	this->setCursor(Qt::ArrowCursor);
        exit(1);
    }

    //first clean the model and the view
    DVSTreeView->cleanup();
    if ( tree )
    {
        try {
        	DVSTreeView->loadDVSComponentTree(tree,partition_name_,manager);
        }
        catch ( CannotLoadDatabase & ex )
        {
            ers::error(ex) ;
            exit(1);
        }
    }
    this->setCursor(Qt::ArrowCursor);

    //reconnect after recreating the model and reactor objects
    connect(thread.instance_->gs, SIGNAL(statusChanged(daq::tmgr::TestResult, std::string )), DVSTreeView->model, SLOT(treeStatusUpdate(daq::tmgr::TestResult, std::string )) );
    connect(thread.instance_->gs, SIGNAL(messageChanged(std::string )), DVSTreeView->model, SLOT(treeItemChanged(std::string  )) );
    connect(DVSTreeView->model, SIGNAL(itemUpdated(std::string )), this, SLOT(updateTabs(std::string )));
//    connect(DVSTreeView->treeView, SIGNAL(clicked(const QModelIndex & )), this, SLOT(printInTab(const QModelIndex &)) );

    connect(DVSTreeView->model, SIGNAL(failedComponent(const QModelIndex &)), DVSTreeView->treeView, SLOT(expandToFailed(const QModelIndex&))) ;

    connect(DVSTreeView->treeView->selectionModel(), SIGNAL(currentChanged(const QModelIndex &, const QModelIndex &)),this, SLOT(printInTab(const QModelIndex &))); 

    connect(DVSTreeView->treeView, SIGNAL(activated(const QModelIndex &)), DVSTreeView->treeView, SLOT(expandSubtree(const QModelIndex&))) ;
    connect(DVSTreeView->treeView, SIGNAL(giveMePosition(const QPoint & )), this, SLOT(showTests(const QPoint &)) );
    connect(DVSTreeView->model, SIGNAL(testingCompleted(std::string )), this, SLOT(enableButtons(std::string )) );
    connect(DVSTreeView->model, SIGNAL(resetDone(std::string )), this, SLOT(enableButtons(std::string )) );
    connect(s_thread.instance_->gs, SIGNAL(statusChanged(daq::tmgr::TestResult, std::string )), DVSTreeView->model, SLOT(treeStatusUpdate(daq::tmgr::TestResult, std::string )) );
    connect(s_thread.instance_->gs, SIGNAL(messageChanged(std::string )), DVSTreeView->model, SLOT(treeItemChanged(std::string  )) );
    statusBar()->showMessage(tr("Database is reloaded."), 2000);

}



std::string DVSMainWindow::getKeyFromCurrentIndex()
{
    QModelIndex index = DVSTreeView->treeView->selectionModel()->currentIndex();
    if(index.isValid()) {
    	QString node = (DVSTreeView->model->data(index,Qt::UserRole)).toString();
	current_node_key_ = node.toStdString();
    }
    else
    	current_node_key_="";
    return current_node_key_;
}

daq::dvs::Component* DVSMainWindow::getComponentFromCurrentIndex()
{
	TreeItem* item = static_cast<TreeItem*>(DVSTreeView->treeView->selectionModel()->currentIndex().internalPointer());
    QVariant v = item->data();
    Component* aa = v.value<Component *>();
    return aa;
}

void DVSMainWindow::startTest()
{
	comp_under_testing=""; //initialize it again
    TreeItem* item = static_cast<TreeItem*>(DVSTreeView->treeView->selectionModel()->currentIndex().internalPointer());
    if(item) {
    	daq::dvs::Component* comp_to_test = getComponentFromCurrentIndex();
    	comp_under_testing=comp_to_test->key();
        if (comp_to_test){
            thread.getTestingParams(manager, comp_to_test, test_scope.toStdString(), test_level);

           //disable other buttons and actions that might involve reloading of CLIPS instances
           actionTest_Component->setDisabled(true);
           actionReset_Component->setDisabled(true);
           actionStop_testing->setEnabled(true);
           actionReload_configuration->setDisabled(true);
           actionTest_level_scope_selector->setDisabled(true);
	   this->setCursor(Qt::BusyCursor);
        }
        else if(item->getStatus() == daq::dvs::Component::Status::UNDEFINED){
           enableButtons(comp_under_testing);
        }
    }
    else {
        QMessageBox::warning(this,tr("Start test"),tr("No valid component is selected."));
        return;
    }
}

void DVSMainWindow::enableButtons(std::string comp)
{
	//std::cout<<"enable buttons: comp= "<<comp<<" comp_under_testing="<<comp_under_testing<<std::endl;
	if ( comp==comp_under_testing ) {
		actionTest_Component->setEnabled(true);
		actionReset_Component->setEnabled(true);
		actionStop_testing->setDisabled(true);
		actionReload_configuration->setEnabled(true);
		actionTest_level_scope_selector->setEnabled(true);

		//QApplication::restoreOverrideCursor();
		this->setCursor(Qt::ArrowCursor);
	}
	if (comp==comp_under_stop) {
		//QApplication::restoreOverrideCursor();
		this->setCursor(Qt::ArrowCursor);
	}
	//special return string in case of Undef status after a Reset
	if (comp=="Reset") {
		//QApplication::restoreOverrideCursor();
		//this->setCursor(Qt::ArrowCursor);
	}

}

void DVSMainWindow::startSingleTest(QAction* action)
{
    QString test_id = action->text();

	daq::dvs::Component* comp_to_test = getComponentFromCurrentIndex();
    if(comp_to_test){
     ERS_INFO("Start single test :"<< test_id.toStdString()<<" for component: "<< comp_to_test->key());
     s_thread.getTestingParams( manager, comp_to_test, test_id, test_scope.toStdString(), test_level );
    }

}

void DVSMainWindow::stopTest()
{
	comp_under_stop="";
	TreeItem* item = static_cast<TreeItem*>(DVSTreeView->treeView->selectionModel()->currentIndex().internalPointer());
    if(item) {
    	daq::dvs::Component* comp_to_stop = getComponentFromCurrentIndex();
    	comp_under_stop = comp_to_stop->key();
    	if (comp_under_stop != ""){
    		ERS_INFO("Call stop "<<comp_under_stop);
    		stop_thread.getStopParams(manager,comp_to_stop);
    	}
    	else if(item->getStatus() == daq::dvs::Component::Status::UNDEFINED){
    		enableButtons(comp_under_stop);
    	}
    }
    else{
     QMessageBox::warning(this,tr("Stop test"),tr("No valid component is selected."));
     return;
    } 
}

void DVSMainWindow::resetState()
{
	comp_under_reset="";comp_under_testing="";
	TreeItem* item = static_cast<TreeItem*>(DVSTreeView->treeView->selectionModel()->currentIndex().internalPointer());
    if(item) {
        Component* comp_to_reset = getComponentFromCurrentIndex();
        comp_under_reset=comp_to_reset->key();
		//if (comp_under_reset != "" && item->getStatus() != daq::dvs::Component::Status::Untested){
        if (comp_under_reset != ""){
		reset_thread.getResetParams(manager,comp_to_reset);
		}
		else if(item->getStatus() == daq::dvs::Component::Status::UNDEFINED){
		   enableButtons(comp_under_reset);
		}
    }
    else{
     QMessageBox::warning(this,tr("Reset test"),tr("No valid component is selected."));
     return;
    } 
}

void DVSMainWindow::showTests(const QPoint & menuPos)
{
    daq::dvs::TestsSet tests_for_obj ;
    try
    {
    	tests_for_obj = manager->getTestsForComponent(getComponentFromCurrentIndex());
    }
    catch   ( ers::Issue& ex)
    {
        ers::error(ex);
    }

    if(tests_for_obj.empty()) {
      return;
    }
    else {
    QAction* act;
    testmenu_ = new QMenu(this);

    for(auto it : tests_for_obj ){
    	std::cout<<it->UID()<<std::endl;
        act = new QAction(this);
        act->setText(QString((it->UID()).c_str()));
        testmenu_->addAction(act);
    }
    connect( testmenu_, SIGNAL(triggered(QAction* )), this, SLOT(startSingleTest(QAction* )) );
    testmenu_->exec(menuPos);
    }
}

void DVSMainWindow::editDB()
{
    ERS_LOG("Start 'oks_data_editor "<<db_name_<<"'");

    QString program = "oks_data_editor";
    QStringList arguments;
    arguments << db_name_;

    QProcess *myProcess = new QProcess();
    myProcess->start(program, arguments);

}

void DVSMainWindow::changeLevelScope()
{
    LevelScopeSelector* levelSel = new LevelScopeSelector(this,test_level,test_scope);
    int code = levelSel->exec();
    if (code==1)
    {   //QDialog::Accepted
    	if(levelSel->comboBox_levelsel->currentText()=="all"){
    		test_level = 255;
    	}
    	else {
    		test_level =(levelSel->comboBox_levelsel->currentText()).toInt();
    	}
        test_scope =(levelSel->comboBox_scope->currentText());
        ERS_LOG("You have set the global testing level to: "<<test_level<<" and scope: "<<test_scope.toStdString());
        manager->setTestScopeLevel(test_scope.toStdString(),test_level);
        QString status="Test level='"+levelSel->comboBox_levelsel->currentText()+"' and scope='"+QString(test_scope)+"'.";
        statusBar()->showMessage(status, 3000);
    }
    else if( code==0 )
    {  //QDialog::Rejected
        ERS_LOG("Unchanged testing level: "<<test_level<<" and scope: "<<test_scope.toStdString());
    }
}

void DVSMainWindow::updateTabs(std::string comp)
{
	printInTab( DVSTreeView->treeView->selectionModel()->currentIndex() );

}

void DVSMainWindow::updateTabs(int type)
{
    // std::cout<<"Update tab index"<<type<<std::endl;
    if ((DVSTreeView->treeView->selectionModel()->currentIndex()).isValid())
        printInTab( DVSTreeView->treeView->selectionModel()->currentIndex() );
    else
        ERS_LOG("No item selected");

}

//fill in the panel tabs
void DVSMainWindow::printInTab(const QModelIndex & ind)
{
//if (DVSTreeView->proxyModel->mapToSource(ind).isValid()) {
//	ERS_LOG("Correct index");
	TreeItem* item = static_cast<TreeItem*>(ind.internalPointer());
    if(item )
    {
        if( (tabWidget->currentWidget())->objectName()=="result" )
        { //TestLog tab
			std::string result_out = item->getTest_out();
        	if(combineChildrenOutput==true){
        		std::string comb_out=combineChildrenOutputForComponent(item, "out");
				result_out+=comb_out;
			}

			if (result_out!="") {
				QString newstr = QString(result_out.c_str());
				newstr.replace("\n<HR>","<HR>");
				newstr.replace("<HR>\n","<HR>");
				newstr.replace("\n","<br>");

				this->test_textout->setHtml(newstr);

				int j=0;int k=0;int m=0;int n=1;
				while ((j = newstr.indexOf("<ps name=\"",j)) != -1) {
				  
				  m=newstr.indexOf("\">",m);
				  k=newstr.indexOf("</ps>",k);
				  
				  QString linkname=newstr.mid(j+10, m-j-10);
				  QString extract=newstr.mid(m+2, k-m-2);
				  QString ref="<a href=\""+extract+"\">View "+linkname+"</a>";

				  switch(n){
				  case 1:
				  	linkFile1->setText(ref);
					break;
				  case 2:	
				  	linkFile2->setText(ref);
					break;
				  case 3:	
				  	linkFile3->setText(ref);
					break;
				  default:
				  	break;	
				  }
				  //QDesktopServices::openUrl(QUrl("file:///home/alina/Docs_applicationForms/list_publications.pdf", QUrl::TolerantMode));
				  ++j;	n++; ++m; ++k;
				}
				
				  linkFile1->setOpenExternalLinks(true);
				  linkFile1->setTextInteractionFlags ( Qt::LinksAccessibleByMouse );
				  linkFile2->setOpenExternalLinks(true);
				  linkFile2->setTextInteractionFlags ( Qt::LinksAccessibleByMouse );				
				  linkFile3->setOpenExternalLinks(true);
				  linkFile3->setTextInteractionFlags ( Qt::LinksAccessibleByMouse );
			}
			else
				this->test_textout->setText("Result is not available yet.");

			std::string result_err = item->getTest_err();
        	if(combineChildrenOutput==true){
        		std::string comb_out=combineChildrenOutputForComponent(item, "err");
				result_err+=comb_out;
			}

			if (result_err!="") {
				QString newstr = QString(result_err.c_str());
				newstr.replace("\n<HR>","<HR>");
				newstr.replace("<HR>\n","<HR>");
				newstr.replace("\n","<br>");
				this->test_texterr->setHtml(newstr);
			}
			else
				this->test_texterr->setText("Result is not available yet.");
        }

        else if ( (tabWidget->currentWidget())->objectName()=="info" )
        {
            Component* comp = item->data().value<Component *>() ;
            if ( comp == nullptr ) return ;

            std::ostringstream info ;
            info << "<H4>Class: " << comp->getClassName() << "</H4>" ;

	        const DalObject* dalobject =  comp->dalObject() ;
            if ( dalobject!=nullptr )
                {
                std::list<const daq::tm::TestPolicy*> policies ;
                DVSManager::getPolicies4Object(policies, dalobject) ;
                info << "<HR><H4>Test policies: </h4>" ;
                for ( const auto tp: policies )
                    {
                    info << "<p><em><font color='brown'>" << tp->UID() << "</font></em>: Synchrounous: " << std::boolalpha << "<font color='blue'>"
                    << tp->get_SynchronousTesting() << "</font>"
                    << "; StopOnError: " << std::boolalpha << "<font color='blue'>" << tp->get_StopOnError() << "</font>"
                    << "; Container test: " << "<font color='blue'>" << tp->get_TestOfContainer()<< "</font>";
                    for ( const auto trel: tp->get_RelationsSequence() )
                        {
                        info << "<p>&#10149;<i>" << trel->get_RelationshipName() 
                        << "</i>: Synchrounous: " << "<font color='blue'>" << std::boolalpha << trel->get_SynchronousTesting() << "</font>"
                        << "; StopOnError: " << std::boolalpha << "<font color='blue'>" << trel->get_StopOnError() << "</font>" ;
                        }
                    }
                }

            this->info_text->setHtml(QString::fromStdString(info.str()));
        }

        else if ( (tabWidget->currentWidget())->objectName()=="diag" )
        {  //Diagnosis tab includes Diagnostics and Actions
            std::string diag = item->getDiagnosis();QString diagHtml="";
            if (diag!=""){
				diagHtml = QString(diag.c_str());
				diagHtml.replace("\n<HR>","<HR>");
				diagHtml.replace("<HR>\n","<HR>");
				diagHtml.replace("\n","<br>");
				diagHtml=QString("<b>Diagnosis:</b><br>")+diagHtml;
            	//this->diag_text->setHtml("<b>Diagnosis:</b><br>"+newstr);
            }
            else {
            	diagHtml="Diagnosis is not available.";
            }
            std::string actions = item->getReason();QString actionsHtml="";
            if (actions!=""){
				actionsHtml = QString(actions.c_str());
				actionsHtml.replace("\n<HR>","<HR>");
				actionsHtml.replace("<HR>\n","<HR>");
				actionsHtml.replace("\n","<br>");
				actionsHtml=QString("<br><b>Actions:</b><br>")+actionsHtml;
            	//this->diag_text->setHtml("<br><b>Actions:</b><br>"+newstr);
            }
            else {
            	actionsHtml="<br>No actions available.";
            }
        	this->diag_text->setHtml(diagHtml+actionsHtml);

         }
        else if ( (tabWidget->currentWidget())->objectName()=="logview" )
        {  //Log Viewer tab
            QString log = "";
            std::string host;
            Component* aa = getComponentFromCurrentIndex();
            //daq::dvs::Component* aa = (item->data()).value<Component *>();
            if(aa)
            {
                const daq::dvs::DVSApplication * dvsApp = dynamic_cast<const daq::dvs::DVSApplication*>(aa);

                if(dvsApp)
                {
                    host = dvsApp->host(); std::cout<<"host "+host<<std::endl;
                }
                else {
//		    const daq::dvs::DVSPmgAgent * dvsPmg = dynamic_cast<const daq::dvs::DVSPmgAgent*>(aa);
//                    if (dvsPmg) {
//			host="not_specified";			
//		    }
//		    else {
		      statusBar()->showMessage(tr("This selected item does not have a logfile."),2000);
                      return;
		    //}
                }
                if ( (tabWidget_logview->currentWidget())->objectName()=="stdout" )
                {
                	log = getLogFile(host,aa->name(),".out", aa->type());
                    this->logview_textout->setPlainText(log);
                }
                else if ( (tabWidget_logview->currentWidget())->objectName()=="stderr" )
                {
                    log = getLogFile(host,aa->name(),".err", aa->type());
                    this->logview_texterr->setPlainText(log);
                }
            }
        }
    }
    else {
        ERS_DEBUG(2,"No valid index");
        return;
    }
//	}else {
//        ERS_DEBUG(2,"No valid index passed from the model.");
//        return;
//	}
}

void DVSMainWindow::logViewerSettings()
{
	QStringList options;
    options << tr("Last log (default)")
            << tr("Last but one log")
            << tr("Last but two log")
            << tr("Last 2000 lines")
            << tr("All log files") ;
    bool ok;
    QString item = QInputDialog::getItem(this, tr("Set log viewer"), tr("Please choose what log you would like to see:"),
                                        options, log_set, false, &ok);
    if (ok && !item.isEmpty()) {
        if (item =="Last log (default)") {
            view_cmd = " cat `ls -t ";
            end_view_cmd = " | head -1`";
            log_set=0;
        }
        else if (item =="Last but one log") {
            view_cmd = " cat `ls -t ";
            end_view_cmd = " | head -2 | tail -1`";
            log_set=1;
		}
        else if (item =="Last but two log") {
            view_cmd = " cat `ls -t ";
            end_view_cmd = " | head -3 | tail -1`";
            log_set=2;
		}
        else if (item =="Last 2000 lines") {
            view_cmd = " tail -n 2000 ` ls -t ";
            end_view_cmd = "";
            log_set=3;
		}
        else if (item =="All log files") {
        	view_cmd = " cat ";
        	end_view_cmd = "";
        	log_set=4;
        }
    }
}

QString DVSMainWindow::getLogFile(std::string host, std::string name, std::string ext, daq::dvs::Component::Type comp_type)
{
    std::string cmdlog;
    cmdlog = "ssh -x "+host+" 'ls -lt "+logroot_+"/"+partition_name_+"/"+name+"_*"+ext;
    cmdlog+=" && "+view_cmd+std::string(logroot_)+"/"+partition_name_+"/"+name+"_*"+ext+end_view_cmd;
    cmdlog+=" || echo Application log does NOT exist' ";
    QString out;
    QProcess *getlog = new QProcess();
    getlog->setReadChannelMode(QProcess::MergedChannels);
    ERS_DEBUG(1,"Start and wait");
    //QApplication::setOverrideCursor(Qt::WaitCursor);
    this->setCursor(Qt::WaitCursor);

    getlog->start("sh",QStringList() << "-c"<<QString(cmdlog.c_str()));
    if(!getlog->waitForStarted(5000))
    {
        ERS_DEBUG(1,"Process not started after 5 seconds");
        getlog->kill();
        //QApplication::restoreOverrideCursor();
	this->setCursor(Qt::ArrowCursor);

        return "";

    }
    ERS_DEBUG(1,"process started");

    if (!getlog->waitForFinished(10000))//10 second timeout to finish
    {
        ERS_DEBUG(1,"process did not finish in time. Terminating process");
        getlog->terminate();
        if (!getlog->waitForFinished(2000))//2 second timeout to terminate
        {
            ERS_DEBUG(1,"Killing process!");
            getlog->kill();
        }
        //blocking method, should use something else
        ERS_DEBUG(1,"errorString() = "<< getlog->errorString().toStdString());
    }
    else
    {
        out = "Reading file with command: "+QString(cmdlog.c_str());
        out+="\n---------------------------------------------------------------------\n";
        out += getlog->readAll();
    }
    //QApplication::restoreOverrideCursor();
    this->setCursor(Qt::ArrowCursor);

    return out;
}

int DVSMainWindow::logPanelSelected()
{
	int panel;
	if ( (tabWidget_testoutput->currentWidget())->objectName()=="stdout_7")
    {
		panel=0;
    }
    else if ( (tabWidget_testoutput->currentWidget())->objectName()=="stderr_7" )
    {
        panel=1;
    }

	return panel;
}
void DVSMainWindow::findFirst(const QString & txt)
{
	if ( logPanelSelected()==0 )
    {
        this->test_textout->moveCursor(QTextCursor::Start);
    }
    else if ( logPanelSelected()==1 )
    {
        this->test_texterr->moveCursor(QTextCursor::Start);
    }
    QTextDocument::FindFlags option;
    if ( this->searchMatchCase_2->isChecked() )
        option = QTextDocument::FindCaseSensitively;
    else
        option = 0;
    if( !txt.isEmpty() )
    {
        if ( (logPanelSelected()==0 && !(this->test_textout->find(txt, option)))
        		|| (logPanelSelected()==1 && !(this->test_texterr->find(txt, option))))
        {
            this->findNextButton_2->setEnabled(false);
            this->findPreviousButton_2->setEnabled(false);
        }
        else
        {
            this->findNextButton_2->setEnabled(true);
            this->findPreviousButton_2->setEnabled(true);
        }
    }
}
void DVSMainWindow::findNext()
{
    QString txt_to_search = this->searchText_2->text();
    QTextDocument::FindFlags option;
    if ( this->searchMatchCase_2->isChecked() )
        option = QTextDocument::FindCaseSensitively;
    else
        option = 0;
    if( !txt_to_search.isEmpty() )
    {
        //if no selection are found anymore, disable nextButton
        if ( (logPanelSelected()==0 && !(this->test_textout->find(txt_to_search, option)))
        		|| (logPanelSelected()==1 && !(this->test_texterr->find(txt_to_search, option))))
        {
            this->findNextButton_2->setEnabled(false);
        }
        else
        {
            this->findNextButton_2->setEnabled(true);
            this->findPreviousButton_2->setEnabled(true);
        }
    }
}

void DVSMainWindow::findPrevious()
{
    QString txt_to_search = this->searchText_2->text();
    QTextDocument::FindFlags option;
    if ( this->searchMatchCase_2->isChecked() )
        option = QTextDocument::FindBackward | QTextDocument::FindCaseSensitively;
    else
        option = QTextDocument::FindBackward;
    if( !txt_to_search.isEmpty() )
    {
        //if no selection are found anymore, disable previousButton
        if ( (logPanelSelected()==0 && !(this->test_textout->find(txt_to_search, option)))
        		|| (logPanelSelected()==1 && !(this->test_texterr->find(txt_to_search, option))))
        {
            this->findPreviousButton_2->setEnabled(false);
        }
        else
        {
            this->findPreviousButton_2->setEnabled(true);
            this->findNextButton_2->setEnabled(true);
        }
    }
}
//replace the corresponding connect method
void DVSMainWindow::on_refreshButton_clicked()
{
    printInTab( DVSTreeView->treeView->selectionModel()->currentIndex() );
}

/**
 * highlights all occurrences of the search text
 */
void DVSMainWindow::highlightSearch()
{
    statusBar()->showMessage(tr("Highlighting..."));
    this->repaint();
    QString txt_to_search = this->searchText_2->text();
    if ( logPanelSelected()==0 )
    {
        highlighter_testout->highlightAll(txt_to_search,this->searchMatchCase_2->isChecked());
    }
    else if ( logPanelSelected()==1 )
    {
    	highlighter_testerr->highlightAll(txt_to_search,this->searchMatchCase_2->isChecked());
    }
    statusBar()->showMessage(tr("Highlighting finished"),2000);
}

void DVSMainWindow::on_browseButton_clicked()
{
	fileName = QFileDialog::getSaveFileName(this,
                                tr("Enter a file name to save test results"),
                                tr("/tmp/TestResultsLog.out") );

    if (fileName.isEmpty()) {
    	if(!filenameEdit->text().isEmpty()) {
    		fileName = filenameEdit->text();
    	}
    	else {
    		QMessageBox::warning(this,tr("Save test results"), tr("No file specified. Results will be saved to /tmp/TestResultsLog.out"));
    		fileName = "/tmp/TestResultsLog.out";
    	}
    }
    filenameEdit->setText(fileName);
}

void DVSMainWindow::on_saveTestResultButton_clicked()
{
    if (fileName.isEmpty()) {
    	if(!filenameEdit->text().isEmpty()) {
    		fileName = filenameEdit->text();
    	}
    	else{
    		int ret = QMessageBox::warning(this, tr("Save test results"), tr("No file specified. Results will be saved to /tmp/TestResultsLog.out\n"
                       "Do you want to continue?"),
                    QMessageBox::Yes | QMessageBox::No,
                    QMessageBox::No);
    		if(ret == QMessageBox::Yes){
    			fileName = "/tmp/TestResultsLog.out";
    		}else
    			return;
    	}
    }
	QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly)) {
    	QMessageBox::warning(this,tr("Save test results"),
	                          tr("Cannot write file %1 because:%2").arg(fileName).arg(file.errorString()));
    	return;
    }
    QTextStream out(&file);
    TreeItem* item = static_cast<TreeItem*>(DVSTreeView->treeView->selectionModel()->currentIndex().internalPointer());
    if(item ) {
    	out << "Test out log: "<<this->test_textout->toPlainText() <<"\nTest err log:"<<this->test_texterr->toPlainText();
    	statusBar()->showMessage(tr("File saved."), 3000);
    }
    else
    	QMessageBox::warning(this,tr("Save test results"),tr("No valid component is selected."));
    file.close();
}

void DVSMainWindow::on_actionViewDVSOutput_triggered()
{
	OutputWindow* dvsout = new OutputWindow(this, std::string(partition_name_));
	dvsout->show();
}


void DVSMainWindow::on_filterButton_clicked(){
	if ( logPanelSelected()==0){
		this->logview_textout->setFilter(this->filterText->text());
	}else{
		this->logview_texterr->setFilter(this->filterText->text());
	}
}

/**
 * Executed when Option Combine test output is checked
 * The combined test out and err are not stored in any structure, they are gathered for display when clicking on a component.
 * and when Test output tab is selected.
 */
void DVSMainWindow::on_actionCombineTestOutput_toggled(bool checked)
{
  if (checked) {
  	ERS_LOG("Activate combining test output.");
  	statusBar()->showMessage(tr("Activate combining test output"), 2000);
  	combineChildrenOutput=true;
  }
  else {
	statusBar()->showMessage(tr("Disable combining test output"), 2000);
	combineChildrenOutput=false;
   }
}

std::string DVSMainWindow::combineChildrenOutputForComponent(TreeItem* item, std::string out_type )
{
	std::string concatenatedChildren="";
	ERS_DEBUG(1, "Combine children output for item "<<item->getKey());
	if(item->childCount()!=0) {
		concatenatedChildren+="<p>Test logs for all subcomponents for "+item->getKey()+" follow:</p>";
		for (int i=0; i<item->childCount(); i++) {
			if(out_type=="out"){
				concatenatedChildren+=item->childItems[i]->getTest_out();
			}
			else if(out_type=="err"){
				concatenatedChildren+=item->childItems[i]->getTest_err();
			}
			if(item->childItems[i]->childCount()!=0){
				concatenatedChildren+=combineChildrenOutputForComponent(item->childItems[i],out_type);
			}
		}
		return concatenatedChildren;
	}
	else
		return "";
}

/**
 * The test output is not available before the callback is coming back.
 * For long tests this method is retrieving the output while the test is running.
 */
void DVSMainWindow::on_updateTestLog_clicked()
{
	TreeItem* item = static_cast<TreeItem*>(DVSTreeView->treeView->selectionModel()->currentIndex().internalPointer());
    std::string test_out="";
    std::string test_err="";

    if(item ) {
    	statusBar()->showMessage(tr("Get runtime test output"), 2000);
    	std::pair<const std::string, const std::string> out_err = manager->getTestLogs(getComponentFromCurrentIndex());
    	test_out = out_err.first;
    	test_err = out_err.second;
     	ERS_INFO("test_out: "<<test_out<<"__");
     	ERS_INFO("test_err: "<<test_err<<"__");
     }
    else {
    	QMessageBox::warning(this,tr("Update ongoing test log"),tr("No valid component selected."));
    	return;
    }

    if ((tabWidget->currentWidget())->objectName()=="result" )
    {  //TestOutput tab
        if ( (tabWidget_testoutput->currentWidget())->objectName()=="stdout_7" )
		{
        	/*if(test_out==""){
        		this->test_textout->setText("Component has no running test. The test is either finished or it was not started.");
        		return;
        	}*/
			this->test_textout->setText(QString::fromStdString(test_out));
		}
		else if ( (tabWidget_testoutput->currentWidget())->objectName()=="stderr_7" )
		{
        	/*if(test_err==""){
        		this->test_texterr->setText("Component has no running test. The test is either finished or it was not started.");
        		return;
        	}*/
			this->test_texterr->setText(QString::fromStdString(test_err));
		}
		else {
	      	QMessageBox::warning(this,tr("Update ongoing test log"),tr("Select TestOutput tab."));
		}
    }
    else{
      	QMessageBox::warning(this,tr("Update ongoing test log"),tr("1. Select the TestOutput tab first.\n2. Start the testing of a component.\n"
						"3. Press 'Update ongoing test log' button on the left bottom of the tab."));
    	return;
    }
}

QString DVSMainWindow::getTestLog(std::string host, std::string test_output, std::string ext)
{
    std::string cmdlog;

    cmdlog = "ssh -x "+host+" less "+test_output+ext;

    QString out;
    QProcess *getlog = new QProcess();
    getlog->setReadChannelMode(QProcess::MergedChannels);
    ERS_DEBUG(1,"Start and wait");
    //QApplication::setOverrideCursor(Qt::WaitCursor);
    this->setCursor(Qt::WaitCursor);

    getlog->start("sh",QStringList() << "-c"<<QString(cmdlog.c_str()));
    if(!getlog->waitForStarted(5000))
    {
        ERS_DEBUG(1,"Process not started after 5 seconds");
        getlog->kill();
        //QApplication::restoreOverrideCursor();
	this->setCursor(Qt::ArrowCursor);

        return "";

    }
    ERS_DEBUG(1,"process started");

    if (!getlog->waitForFinished(10000))//10 second timeout to finish
    {
        ERS_DEBUG(1,"process did not finish in time. Terminating process");
        getlog->terminate();
        if (!getlog->waitForFinished(2000))//2 second timeout to terminate
        {
            ERS_DEBUG(1,"Killing process!");
            getlog->kill();
        }
        //blocking method, should use something else
        ERS_DEBUG(1,"errorString() = "<< getlog->errorString().toStdString());
    }
    else
    {
        out = "Reading file with command: "+QString(cmdlog.c_str());
        out+="\n---------------------------------------------------------------------\n";
        out += getlog->readAll();
    }
    //QApplication::restoreOverrideCursor();
    this->setCursor(Qt::ArrowCursor);

    return out;
}

void DVSMainWindow::on_actionHelpMenu_triggered()
{
	HelpBrowser::showPage("index.html");
}

/**
 * sets up the highlighters
 */
void DVSMainWindow::setupHighlighters()
{
    highlighter = new daq::QTUtils::Highlighter(logview_textout->document());
    highlighter_err = new daq::QTUtils::Highlighter(logview_texterr->document());
    highlighter_testout = new daq::QTUtils::Highlighter(test_textout->document());
    highlighter_testerr = new daq::QTUtils::Highlighter(test_texterr->document());

    /*
     * Setup common formatting first (color of text, function recognition,etc)
     */
    QTextCharFormat format;
    QRegExp pattern;
    format.setForeground(Qt::darkBlue);
    logview_textout->setCurrentCharFormat(format);
    logview_texterr->setCurrentCharFormat(format);
    test_textout->setCurrentCharFormat(format);
    test_texterr->setCurrentCharFormat(format);
    //highlight up to message
    QTextCharFormat upToMessage;
    upToMessage.setForeground(Qt::black);
    pattern = QRegExp("^.*\\[[^\\[\\]]*\\]\\s");
    highlighter->setRule(pattern,upToMessage);
    highlighter_err->setRule(pattern,upToMessage);

    //highlight functions
    format.setForeground(Qt::black);
    format.setFontWeight(QFont::Bold);
    pattern = QRegExp("[^\\[]*::\\S*\\(.*\\)");
    pattern.setMinimal(true);
    highlighter->setRule(pattern,format);
    highlighter_err->setRule(pattern,format);

    /*
     * test result log specific formatting
     */
    format.setForeground(Qt::gray);
    format.setFontWeight(QFont::Bold);
    pattern = QRegExp("Test logs for all subcomponents for");
    highlighter_testout->setRule(pattern, format);
    highlighter_testerr->setRule(pattern, format);

    format.setForeground(Qt::darkRed);
    pattern = QRegExp("FAILED");
    highlighter_testout->setRule(pattern, format);
    highlighter_testerr->setRule(pattern, format);

    format.setForeground(Qt::darkGreen);
    pattern = QRegExp("PASSED");
    highlighter_testout->setRule(pattern, format);
    highlighter_testerr->setRule(pattern, format);

    format.setForeground(Qt::darkYellow);
    pattern = QRegExp("NOTDONE");
    highlighter_testout->setRule(pattern, format);
    highlighter_testerr->setRule(pattern, format);

    QTextCharFormat testout;
    testout.setForeground(Qt::darkBlue);
    //pattern = QRegExp("<HR(.*)<HR>",Qt::CaseInsensitive);
    pattern = QRegExp("^.*output:");
    highlighter_testout->setRule(pattern, testout);
    highlighter_testerr->setRule(pattern, testout);

    /*
     * stdout specific formatting
     */

    //highlight log files
    format.setForeground(Qt::darkGreen);
    pattern = QRegExp("^.*\\.out$");
    highlighter->setRule(pattern,format);

    //highlight log messages
    format.setForeground(Qt::white);
    format.setBackground(Qt::lightGray);
    pattern = QRegExp("^LOG");
    highlighter->setRule(pattern,format);

    //highlight info messages
    format.setForeground(Qt::white);
    format.setBackground(Qt::blue);
    pattern = QRegExp("^INFO");
    highlighter->setRule(pattern,format);

    /*
     * stderr specific formatting
     */

    QTextCharFormat errorFormat;
    QRegExp errorPattern;

    //log files
    errorFormat.setForeground(Qt::darkGreen);
    errorPattern = QRegExp("^.*\\.err$");
    highlighter_err->setRule(errorPattern,errorFormat);

    //use white text for WARNING,ERROR,FATAL text
    errorFormat.setFontWeight(QFont::Bold);
    errorFormat.setForeground(Qt::black);
    errorFormat.setBackground(Qt::yellow);
    errorPattern = QRegExp("^WARNING");
    highlighter_err->setRule(errorPattern,errorFormat);

    //red background for ERROR text (only at start of sentence)

    errorFormat.setForeground(Qt::white);
    errorFormat.setBackground(Qt::red);
    errorPattern = QRegExp("^ERROR");
    highlighter_err->setRule(errorPattern,errorFormat);

    //dark red background and bold font for FATAL (only at start of sentence)
    errorFormat.setBackground(Qt::darkRed);
    errorPattern = QRegExp("^FATAL");
    highlighter_err->setRule(errorPattern,errorFormat);

}

/**
 * Set ON/OFF the verbosity of the test output.
 */
void DVSMainWindow::on_actionTest_verbosity_toggled(bool checked){
	  if (checked) {
	  	ERS_LOG("Test verbosity is set to OFF.");
	  	manager->setTestVerbosity(false);
	  	statusBar()->showMessage(tr("Test verbosity is OFF."), 2000);
	  }
	  else {
		statusBar()->showMessage(tr("Test verbosity is ON."), 2000);
		manager->setTestVerbosity(true);
	  }
}

/**
 * Discard retrieval of the test output at all.
 */
void DVSMainWindow::on_actionDiscard_test_output_toggled(bool checked){
	  if (checked) {
	  	ERS_LOG("Test output is discarded.");
	  	manager->discardTestOutput(true);
	  	statusBar()->showMessage(tr("Test output is discarded."), 2000);
	  }
	  else {
		manager->discardTestOutput(false);
	  }
}

/**
 * Set ON/OFF the HW view of the tree
 */
void DVSMainWindow::on_actionHWView_toggled(bool checked){
	  if (checked) {
		HWview=true;
ERS_LOG("checked");
	  }
	  else {
		HWview=false;
ERS_LOG("not checked");
	  }
	  reloadDB(HWview);

}
